import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class UtilsTest {

    @Test
    public void dividirInfoPorNumero() {
        DataUnit dataUnit = new DataUnit(2, 2, 2, 2, 2);

        DataUnit esperado = new DataUnit(1, 1, 1, 1, 1);
        DataUnit resultado = Utils.dividirInfoPorNumero(dataUnit, 2);

        assertEquals(esperado.dailyNotInfected, resultado.dailyNotInfected, 0);
        assertEquals(esperado.infected, resultado.infected, 0);
        assertEquals(esperado.hospitalized, resultado.hospitalized, 0);
        assertEquals(esperado.UCI_Hospitalized, resultado.UCI_Hospitalized, 0);
        assertEquals(esperado.deaths, resultado.deaths, 0);
    }

    @Test
    public void multiplicarInfos() {
        DataUnit dataUnit = new DataUnit(2, 2, 2, 2, 2);
        DataUnit dataUnit2 = new DataUnit(3, 3, 3, 3, 3);

        DataUnit esperado = new DataUnit(6, 6, 6, 6, 6);
        DataUnit resultado = Utils.multiplicarInfos(dataUnit, dataUnit2);

        assertEquals(esperado.dailyNotInfected, resultado.dailyNotInfected, 0);
        assertEquals(esperado.infected, resultado.infected, 0);
        assertEquals(esperado.hospitalized, resultado.hospitalized, 0);
        assertEquals(esperado.UCI_Hospitalized, resultado.UCI_Hospitalized, 0);
        assertEquals(esperado.deaths, resultado.deaths, 0);
    }

    @Test
    public void somarInfo() {
        DataUnit dataUnit = new DataUnit(2, 2, 2, 2, 2);
        DataUnit dataUnit2 = new DataUnit(3, 3, 3, 3, 3);

        DataUnit esperado = new DataUnit(5, 5, 5, 5, 5);
        DataUnit resultado = Utils.somarInfo(dataUnit, dataUnit2);

        assertEquals(esperado.dailyNotInfected, resultado.dailyNotInfected, 0);
        assertEquals(esperado.infected, resultado.infected, 0);
        assertEquals(esperado.hospitalized, resultado.hospitalized, 0);
        assertEquals(esperado.UCI_Hospitalized, resultado.UCI_Hospitalized, 0);
        assertEquals(esperado.deaths, resultado.deaths, 0);
    }

    @Test
    public void subtrairInfo() {
        DataUnit dataUnit = new DataUnit(2, 2, 2, 2, 2);
        DataUnit dataUnit2 = new DataUnit(3, 3, 3, 3, 3);

        DataUnit esperado = new DataUnit(1, 1, 1, 1, 1);
        DataUnit resultado = Utils.subtrairInfo(dataUnit2, dataUnit);

        assertEquals(esperado.dailyNotInfected, resultado.dailyNotInfected, 0);
        assertEquals(esperado.infected, resultado.infected, 0);
        assertEquals(esperado.hospitalized, resultado.hospitalized, 0);
        assertEquals(esperado.UCI_Hospitalized, resultado.UCI_Hospitalized, 0);
        assertEquals(esperado.deaths, resultado.deaths, 0);
    }

    @Test
    public void verificarDatas() {
        LocalDate data1 = LocalDate.of(2022, 2, 1);
        LocalDate data2 = LocalDate.of(2022, 2, 10);

        boolean esperado = true;
        boolean resultado = Utils.verificarDatas(data1, data2);

        assertEquals(esperado, resultado);

        esperado = false;
        resultado = Utils.verificarDatas(data2, data2);

        assertEquals(esperado, resultado);

        resultado = Utils.verificarDatas(data2, data1);

        assertEquals(esperado, resultado);
    }

    @Test
    public void diferencaEmDias() {
        LocalDate data1 = LocalDate.of(2022, 2, 1);
        LocalDate data2 = LocalDate.of(2022, 2, 10);

        int esperado = 9;
        int resultado = Utils.diferencaEmDias(data2, data1);

        assertEquals(esperado, resultado);

        esperado = 0;
        resultado = Utils.diferencaEmDias(data1, data1);

        assertEquals(esperado, resultado);
    }

    @Test
    public void raizQuadrada() {
        DataUnit dataUnit = new DataUnit(4, 4, 4, 4, 4);

        DataUnit esperado = new DataUnit(2, 2, 2, 2, 2);
        DataUnit resultado = Utils.raizQuadrada(dataUnit);

        assertEquals(esperado.dailyNotInfected, resultado.dailyNotInfected, 0);
        assertEquals(esperado.infected, resultado.infected, 0);
        assertEquals(esperado.hospitalized, resultado.hospitalized, 0);
        assertEquals(esperado.UCI_Hospitalized, resultado.UCI_Hospitalized, 0);
        assertEquals(esperado.deaths, resultado.deaths, 0);
    }

    @Test
    public void mediaArrayTotal() {
        DataUnit[] array = {new DataUnit(1, 1, 1, 1, 1), new DataUnit(2, 2, 2, 2, 2),
                new DataUnit(3, 3, 3, 3, 3), new DataUnit(4, 4, 4, 4, 4)};

        DataUnit esperado = Utils.dividirInfoPorNumero(new DataUnit(5, 5, 5, 5, 5), 2);
        DataUnit resultado = Utils.mediaArrayTotal(array);

        assertEquals(esperado.dailyNotInfected, resultado.dailyNotInfected, 0);
        assertEquals(esperado.infected, resultado.infected, 0);
        assertEquals(esperado.hospitalized, resultado.hospitalized, 0);
        assertEquals(esperado.UCI_Hospitalized, resultado.UCI_Hospitalized, 0);
        assertEquals(esperado.deaths, resultado.deaths, 0);
    }

    @Test
    public void desvioPadraoDiferencas() {
        DataUnit[] array = {new DataUnit(1, 1, 1, 1, 1), new DataUnit(2, 2, 2, 2, 2),
                new DataUnit(3, 3, 3, 3, 3), new DataUnit(4, 4, 4, 4, 4)};

        DataUnit esperado = Utils.dividirInfoPorNumero(new DataUnit(1118, 1118, 1118, 1118, 1118), 1000);
        DataUnit media = Utils.mediaArrayTotal(array);
        DataUnit resultado = Utils.desvioPadraoDiferencas(array, media);

        assertEquals(esperado.dailyNotInfected, resultado.dailyNotInfected, 0.01);
        assertEquals(esperado.infected, resultado.infected, 0.01);
        assertEquals(esperado.hospitalized, resultado.hospitalized, 0.01);
        assertEquals(esperado.UCI_Hospitalized, resultado.UCI_Hospitalized, 0.01);
        assertEquals(esperado.deaths, resultado.deaths, 0.01);
    }
}