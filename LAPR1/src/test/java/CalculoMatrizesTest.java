import org.junit.Test;

import static org.junit.Assert.*;

public class CalculoMatrizesTest {

    @Test
    public void decomporMatrizComMetodoCrout() {

    }

    @Test
    public void subtrairMatrizez() {
        float[][] matriz1 = {{3, 3, 3, 3}, {3, 3, 3, 3}, {3, 3, 3, 3}, {3, 3, 3, 3}};
        float[][] matriz2 = {{2, 2, 2, 2}, {2, 2, 2, 2}, {2, 2, 2, 2}, {2, 2, 2, 2}};

        float[][] esperado = {{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};

        float[][] resultado = CalculoMatrizes.subtrairMatrizes(matriz1, matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    public void somarMatrizez() {
        float[][] matriz1 = {{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};
        float[][] matriz2 = {{2, 2, 2, 2}, {2, 2, 2, 2}, {2, 2, 2, 2}, {2, 2, 2, 2}};

        float[][] esperado = {{3, 3, 3, 3}, {3, 3, 3, 3}, {3, 3, 3, 3}, {3, 3, 3, 3}};

        float[][] resultado = CalculoMatrizes.somarMatrizes(matriz1, matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    public void multiplicarMatriz() {
        float[][] matriz1 = {{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};
        float[][] matriz2 = {{2, 2, 2, 2}, {2, 2, 2, 2}, {2, 2, 2, 2}, {2, 2, 2, 2}};

        float[][] esperado = {{8, 8, 8, 8}, {8, 8, 8, 8}, {8, 8, 8, 8}, {8, 8, 8, 8}};

        float[][] resultado = CalculoMatrizes.multiplicarMatriz(matriz1, matriz2);

        assertArrayEquals(esperado, resultado);
    }

    @Test
    public void determinante() {
        float[][] matriz1 = {{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}};

        float esperado = 0;
        float resultado = CalculoMatrizes.determinante(matriz1, 4);

        assertEquals(esperado, resultado, 0.05);

        float[][] matriz2 = {{1, 2, 3, 4}, {2, 1, 2, 3}, {3, 2, 1, 2}, {4, 3, 2, 1}};

        esperado = -20;
        resultado = CalculoMatrizes.determinante(matriz2, 4);

        assertEquals(esperado, resultado, 0.05);
    }

    @Test
    public void adjunta() {
        float[][] matriz1 = {{1, 2, 3, 4}, {2, 1, 2, 3}, {3, 2, 1, 2}, {4, 3, 2, 1}};

        float[][] esperado = {{8, -10, 0, -2}, {-10, 20, -10, 0}, {0, -10, 20, -10}, {-2, 0, -10, 8}};
        float[][] resultado = CalculoMatrizes.adjunta(matriz1);

        for (int i = 0; i < esperado.length; i++) {
            for (int j = 0; j < esperado[i].length; j++) {
                assertEquals(esperado[i][j], resultado[i][j], 0.0);
            }
        }
    }

    @Test
    public void matrizInversa() {
        float[][] matriz1 = {{1, 2, 3, 4}, {2, 1, 2, 3}, {3, 2, 1, 2}, {4, 3, 2, 1}};

        float[][] esperado = {{-0.4f, 0.5f, 0f, 0.1f}, {0.5f, -1, 0.5f, 0}, {0, 0.5f, -1, 0.5f}, {0.1f, 0, 0.5f, -0.4f}};
        float[][] resultado = CalculoMatrizes.matrizInversa(matriz1);

        for (int i = 0; i < esperado.length; i++) {
            for (int j = 0; j < esperado[i].length; j++) {
                assertEquals(esperado[i][j], resultado[i][j], 0.0);
            }
        }
    }
}