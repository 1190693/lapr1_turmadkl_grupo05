public class CalculoMatrizes {
    public static float[][] matrizTransicoesEstado = new float[5][5];
    public static float[][] matrizL = new float[5][5];
    public static float[][] matrizU = new float[5][5];

    public static void loadMatrizTransicoesEstado(String[] dataArray) {
        int posicaoColuna = 0, posicaoLinha = 0;
        float valorProbabilidade = 0f;
        for (String line : dataArray) {
            if (line.length() > 0) {
                String probabilidadeString = line.split("=")[1];
                valorProbabilidade = Float.parseFloat(probabilidadeString);

                posicaoLinha = Integer.parseInt(String.valueOf(line.charAt(1))) - 1;
                posicaoColuna = Integer.parseInt(String.valueOf(line.charAt(2))) - 1;
                matrizTransicoesEstado[posicaoLinha][posicaoColuna] = valorProbabilidade;
            }
        }

        //decomporMatrizComMetodoCrout();
    }

    /**
     * @param matrizTransicoesEstado
     * @return um float[][][] em q float[0] é a matriz L e float[1] é a matriz U
     */
    public static float[][][] decomporMatrizComMetodoCrout(float[][] matrizTransicoesEstado) {
        float[][][] matrizesLU = new float[2][matrizTransicoesEstado.length][matrizTransicoesEstado[0].length];
        float[][] matrizL = new float[matrizTransicoesEstado.length][matrizTransicoesEstado[0].length];
        float[][] matrizU = new float[matrizTransicoesEstado.length][matrizTransicoesEstado[0].length];

        for (int i = 0; i < matrizTransicoesEstado.length; i++) {
            // Começar por preencher a matriz L
            for (int j = 0; j < matrizTransicoesEstado.length; j++) {
                // Neste ciclo for j é a linha e i é a coluna

                // Se a linha for menor que a coluna então meter esse valor a 0 (diagonal superior)
                if (j < i)
                    matrizL[j][i] = 0;
                else {

                    matrizL[j][i] = matrizTransicoesEstado[j][i];
                    for (int k = 0; k < i; k++) {
                        matrizL[j][i] = matrizL[j][i] - matrizL[j][k] * matrizU[k][i];
                    }
                }
            }

            // Depois da matriz L estar preenchida preencher a matriz U
            for (int j = 0; j < matrizTransicoesEstado.length; j++) {
                // Neste ciclo for i é a linha e j é a coluna

                // Se a linha for maior que a coluna então meter esse valor a 0 (diagonal inferior)
                if (j < i)
                    matrizU[i][j] = 0;
                    // Se a linha for igual à coluna então meter esse valor a 1 (diagonal)
                else if (j == i)
                    matrizU[i][j] = 1;
                else {
                    matrizU[i][j] = matrizTransicoesEstado[i][j] / matrizL[i][i];
                    for (int k = 0; k < i; k++) {
                        matrizU[i][j] = matrizU[i][j] - ((matrizL[i][k] * matrizU[k][j]) / matrizL[i][i]);
                    }
                }
            }
        }

        matrizesLU[0] = matrizL;
        matrizesLU[1] = matrizU;

        return matrizesLU;
    }

    public static float[][] subtrairMatrizes(float[][] matriz1, float[][] matriz2) {
        float[][] matrizResultado = new float[matriz1.length][matriz1[0].length];

        if (matriz1.length == matriz2.length && matriz1[0].length == matriz2[0].length)
            for (int i = 0; i < matriz1.length; i++) {
                for (int j = 0; j < matriz1.length; j++) {
                    matrizResultado[i][j] = matriz1[i][j] - matriz2[i][j];
                }
            }

        return matrizResultado;
    }

    public static float[][] somarMatrizes(float[][] matriz1, float[][] matriz2) {
        float[][] matrizResultado = new float[matriz1.length][matriz1[0].length];

        if (matriz1.length == matriz2.length && matriz1[0].length == matriz2[0].length)
            for (int i = 0; i < matriz1.length; i++) {
                for (int j = 0; j < matriz1.length; j++) {
                    matrizResultado[i][j] = matriz1[i][j] + matriz2[i][j];
                }
            }

        return matrizResultado;
    }

    public static float[][] multiplicarMatriz(float[][] MatrizP, float[][] Matriz2) {
        float resultado[][] = new float[MatrizP.length][Matriz2[0].length];
        for (int i = 0; i < MatrizP.length; i++) {
            for (int j = 0; j < Matriz2[0].length; j++) {
                for (int k = 0; k < MatrizP[0].length; k++)
                    resultado[i][j] += MatrizP[i][k] * Matriz2[k][j];
            }
        }
        return resultado;
    }

    /**
     * Metodo para obter a submatrz de matriz sem a linha linhaIgnorar e a coluna colunaIgnorar
     * @param matriz matriz a partir da qual se obtém a sub matriz
     * @param linhaIgnorar linha de matriz que vai ser removida
     * @param colunaIgnorar coluna de matriz que vai ser removida
     * @param n tamanho da matriz
     * @return sumatriz com o mesmo tamanho de matriz mas com a submatriz posicionada no canto superior esquerdo
     */
    public static float[][] subMatriz(float[][] matriz, int linhaIgnorar, int colunaIgnorar, int n) {
        float[][] submatriz = new float[matriz.length][matriz.length];
        int i = 0, j = 0;

        // Percorre a matriz
        for (int linha = 0; linha < n; linha++) {
            for (int col = 0; col < n; col++) {

                // Se não for a linha a ignorar e a coluna a ignorar passar esse elemento para a submatriz
                if (linha != linhaIgnorar && col != colunaIgnorar) {
                    submatriz[i][j++] = matriz[linha][col];

                    if (j == n - 1) {
                        j = 0;
                        i++;
                    }
                }
            }
        }
        return submatriz;
    }


    /**
     * Cálculo do determinante da matriz usando o formula descrita em:
     * https://pt.wikipedia.org/wiki/Determinante
     *
     * @param matriz
     * @param n
     * @return
     */
    public static float determinante(float[][] matriz, int n) {
        float resultado = 0; // Initialize result

        // condição de paragem da recursividade
        if (n == 1)
            return matriz[0][0];

        // Matriz para guardar a submatriz
        float[][] submatriz = new float[matriz.length][matriz.length];

        int sinal = 1;

        // Percorrer cada elemento da primeira linha
        for (int f = 0; f < n; f++) {
            submatriz = subMatriz(matriz, 0, f, n);
            // Multiplicar o elemento da linha 0 e coluna f com o determinate da submatriz sem essa linha e coluna
            resultado += sinal * matriz[0][f] * determinante(submatriz, n - 1);


            // alternar o sinal
            sinal = -sinal;
        }

        return resultado;
    }

    public static float[][] adjunta(float[][] matriz) {
        float[][] adj = new float[matriz.length][matriz.length];
        if (matriz.length == 1) {
            adj[0][0] = 1;
            return null;
        }

        int sinal = 1;
        float[][] submatriz = new float[matriz.length][matriz.length];

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {

                submatriz = subMatriz(matriz, i, j, matriz.length);

                if ((i + j) % 2 == 0) {
                    sinal = 1;
                } else {
                    sinal = -1;
                }

                adj[j][i] = (sinal) * (determinante(submatriz, matriz.length - 1));
            }
        }
        return adj;
    }

    public static float[][] matrizInversa(float[][] matriz) {
        float[][] inversa = new float[matriz.length][matriz.length];

        float det = determinante(matriz, matriz.length);
        if (det == 0) {
            System.out.println("Matriz singular, impossível calcular a sua inversa");
        }

        float[][] adj = adjunta(matriz);

        // Inversa usando a formula "inversa(A) = adjunta(A)/determinante(A)"
        for (int i = 0; i < matriz.length; i++)
            for (int j = 0; j < matriz.length; j++)
                inversa[i][j] = adj[i][j] / det;

        return inversa;
    }
}
