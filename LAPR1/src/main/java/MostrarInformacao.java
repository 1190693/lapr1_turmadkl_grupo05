import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class MostrarInformacao {
    public static String[] mostrarInformacaoNovosCasos(DataUnit info, int option) {
        String[] texto = new String[1];
        switch (option) {
            case 1:
                texto[0] = String.format("\tNovos casos: %.0f", info.infected);
                break;
            case 2:
                texto[0] = String.format("\tNovas hospilatizações: %.0f", info.hospitalized);
                break;
            case 3:
                texto[0] = String.format("\tNovos internados em UCI: %.0f", info.UCI_Hospitalized);
                break;
            case 4:
                texto[0] = String.format("\tNovas mortes: %.0f", info.deaths);
                break;
            default:
                texto = new String[5];
                texto[0] = String.format("\tNovos casos: %.0f", info.infected);
                texto[1] = String.format("\tNovas hospilatizações: %.0f", info.hospitalized);
                texto[2] = String.format("\tNovos internados em UCI: %.0f", info.UCI_Hospitalized);
                texto[3] = String.format("\tNovas mortes: %.0f", info.deaths);
                texto[4] = "";
                break;
        }
        /*if (Menu.modoIterativo) {
            for (String s : texto) {
                System.out.println(s);
            }
        }*/

        return texto;
    }

    public static String[] mostarInformacaoDadosTotais(DataUnit info, int option) {
        String[] texto = new String[1];
        switch (option) {
            case 1:
                texto[0] = String.format("\tTotal de infetados: %.0f", info.infected);
                break;
            case 2:
                texto[0] = String.format("\tTotal de hospilatizações: %.0f", info.hospitalized);
                break;
            case 3:
                texto[0] = String.format("\tTotal de internados em UCI: %.0f", info.UCI_Hospitalized);
                break;
            case 4:
                texto[0] = String.format("\tTotal de mortes: %.0f", info.deaths);
                break;
            default:
                texto = new String[5];
                texto[0] = String.format("\tTotal de infetados: %.0f", info.infected);
                texto[1] = String.format("\tTotal de hospilatizações: %.0f", info.hospitalized);
                texto[2] = String.format("\tTotal de internados em UCI: %.0f", info.UCI_Hospitalized);
                texto[3] = String.format("\tTotal de mortes: %.0f", info.deaths);
                texto[4] = "";
                break;
        }
        /*if (Menu.modoIterativo) {
            for (String s : texto) {
                System.out.println(s);
            }
        }*/

        return texto;
    }

    public static String[] mostrarInformacaoPrevisaoDados(float[][] arrayDadosDiaPrevisao, int option) {
        String[] texto = new String[1];
        switch (option) {
            case 1:
                texto[0] = String.format("\tTotal de não infetados: %.1f", arrayDadosDiaPrevisao[0][0]);
                break;
            case 2:
                texto[0] = String.format("\tTotal de infetados: %.1f", arrayDadosDiaPrevisao[1][0]);
                ;
                break;
            case 3:
                texto[0] = String.format("\tTotal de hospilatizações: %.1f", arrayDadosDiaPrevisao[2][0]);
                break;
            case 4:
                texto[0] = String.format("\tTotal de internados em UCI: %.1f", arrayDadosDiaPrevisao[3][0]);
                break;
            case 5:
                texto[0] = String.format("\tTotal de óbitos: %.1f", arrayDadosDiaPrevisao[4][0]);
                break;
            default:
                texto = new String[6];
                texto[0] = String.format("\tTotal de não infetados: %.1f", arrayDadosDiaPrevisao[0][0]);
                texto[1] = String.format("\tTotal de infetados: %.1f", arrayDadosDiaPrevisao[1][0]);
                texto[2] = String.format("\tTotal de hospilatizações: %.1f", arrayDadosDiaPrevisao[2][0]);
                texto[3] = String.format("\tTotal de internados em UCI: %.1f", arrayDadosDiaPrevisao[3][0]);
                texto[4] = String.format("\tTotal de mortes: %.1f", arrayDadosDiaPrevisao[4][0]);
                texto[5] = "";
        }
        /*if (Menu.modoIterativo) {
            for (String s : texto) {
                System.out.println(s);
            }
        }*/
        return texto;
    }

    public static String[] mostrarInformacaoComDecimasNovosCasos(DataUnit info, int option) {
        String[] texto = new String[1];
        switch (option) {
            case 1:
                texto[0] = String.format("\tNovos casos: %.4f", info.infected);
                break;
            case 2:
                texto[0] = String.format("\tNovas hospilatizações: %.4f", info.hospitalized);
                break;
            case 3:
                texto[0] = String.format("\tNovos internados em UCI: %.4f", info.UCI_Hospitalized);
                break;
            case 4:
                texto[0] = String.format("\tNovas mortes: %.4f", info.deaths);
                break;
            default:
                texto = new String[5];
                texto[0] = String.format("\tNovos casos: %.4f", info.infected);
                texto[1] = String.format("\tNovas hospilatizações: %.4f", info.hospitalized);
                texto[2] = String.format("\tNovos internados em UCI: %.4f", info.UCI_Hospitalized);
                texto[3] = String.format("\tNovas mortes: %.4f", info.deaths);
                texto[4] = "";
        }
        /*if (Menu.modoIterativo) {
            for (String s : texto) {
                System.out.println(s);
            }
        }*/

        return texto;
    }

    public static String[] mostrarInformacaoComDecimasTotal(DataUnit info, int option) {
        String[] texto = new String[1];
        switch (option) {
            case 1:
                texto[0] = String.format("\tTotal de Infetados: %.4f", info.infected);
                break;
            case 2:
                texto[0] = String.format("\tTotal de hospilatizações: %.4f", info.hospitalized);
                break;
            case 3:
                texto[0] = String.format("\tTotal de internados em UCI: %.4f", info.UCI_Hospitalized);
                break;
            case 4:
                texto[0] = String.format("\tTotal de mortes: %.4f", info.deaths);
                break;
            default:
                texto = new String[5];
                texto[0] = String.format("\tTotal de Infetados: %.4f", info.infected);
                texto[1] = String.format("\tTotal de hospitalizações: %.4f", info.hospitalized);
                texto[2] = String.format("\tTotal de internados em UCI: %.4f", info.UCI_Hospitalized);
                texto[3] = String.format("\tTotal de mortes: %.4f", info.deaths);
                texto[4] = "";
        }
        /*if (Menu.modoIterativo) {
            for (String s : texto) {
                System.out.println(s);
            }
        }*/

        return texto;
    }

    public static String mostrarDataDiario(LocalDate diario) {
        String output = "\tData: " + diario.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        /*if (Menu.modoIterativo)
            System.out.println(output);*/

        return output;
    }

    public static String mostarDataDadosTotais(LocalDate diario) {
        String output = "\tCasos totais ativos em: " + diario.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        /*if (Menu.modoIterativo)
            System.out.println(output);*/
        return output;
    }

    public static String mostrarIntervaloTempo(LocalDate dataInicio, LocalDate dataFim) {
        String data = "\tIntervalo entre " + dataInicio.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))
                + " e " + dataFim.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + ":";
        /*if (Menu.modoIterativo) {
            System.out.println(data);
        }*/
        return data;
    }

    public static String mostrarSemana(LocalDate primeiroDiaSemana, LocalDate ultimoDiaSemana) {
        String output = "Semana do dia " + primeiroDiaSemana.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))
                + " ao dia " + ultimoDiaSemana.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        /*if (Menu.modoIterativo)
            System.out.println(output);*/
        return output;
    }

    public static String mostrarMes(LocalDate primeiroDiaDoMes) {
        String data;
        if (primeiroDiaDoMes.getMonthValue() < 10) {
            data = "Mês: 0" + primeiroDiaDoMes.getMonthValue();
        } else {
            data = "Mês: " + primeiroDiaDoMes.getMonthValue();
        }

        /*if (Menu.modoIterativo)
            System.out.println(data);*/

        return data;
    }

    public static String[] formarOutput(String titulo, String data, String[] valores) {
        String[] output = new String[1000];
        int i = 0;
        output[i] = "==================";
        i++;
        output[i] = titulo;
        i++;
        output[i] = data;
        i++;
        for (String valor : valores) {
            output[i] = valor;
            i++;
        }
        output[i] = "==================";

        return Arrays.copyOfRange(output, 0, i + 1);
    }

    public static String[] formarOutput(String titulo, String[] valores) {
        String[] output = new String[1000];
        int i = 0;
        output[i] = "==================";
        i++;
        output[i] = titulo;
        i++;
        for (String valor : valores) {
            output[i] = valor;
            i++;
        }
        output[i] = "==================";

        return Arrays.copyOfRange(output, 0, i + 1);
    }

    public static String[] mostrarDiasAteMorte(float[][] matrizM) {
        String[] output = new String[7];
        int i = 0;
        output[i] = "==================";
        i++;
        output[i] = "Número esperado de dias até à morte:";
        i++;
        output[i] = String.format("\tNão infetado: %.1f dias", matrizM[0][0]);
        i++;
        output[i] = String.format("\tInfetado: %.1f dias", matrizM[0][1]);
        i++;
        output[i] = String.format("\tHospitalizado: %.1f dias", matrizM[0][2]);
        i++;
        output[i] = String.format("\tInternado em UCI: %.1f dias", matrizM[0][3]);
        i++;
        output[i] = "==================";

        /*if (Menu.modoIterativo) {
            for (String s : output) {
                System.out.println(s);
            }
        }*/

        return output;
    }

    public static String mostraPrevisaoDadosData(LocalDate diaPrevisao) {
        String data = "\tData: " + diaPrevisao.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        /*if (Menu.modoIterativo)
            System.out.println(data);*/
        return data;
    }

    public static String mostrarDatasDiferenca(LocalDate data1, LocalDate data2) {
        String data = "Diferença entre: " + data1.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + " e "
                + data2.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        /*if (Menu.modoIterativo)
            System.out.println(data);*/

        return data;
    }
}
