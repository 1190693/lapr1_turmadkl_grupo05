public class Main {
    public static void main(String[] args) {
        if (args.length == 0)
            Menu.run();
        else
            ModoNaoInterativo.interpretarArgumentos(args);
    }
}
