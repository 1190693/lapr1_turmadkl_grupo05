import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Scanner;

public class FileReader {
    public static String[] readCSV_File(String path) {

        var pathParameters = path.split("\\.");
        if (pathParameters.length < 2) {
            throw new IllegalArgumentException("Caminho do ficheiro incorreto");
        }
        String extension = pathParameters[pathParameters.length - 1];
        if (!extension.contains("csv")) {
            throw new IllegalArgumentException("Apenas aceita ficheiros CSV.");
        }

        return read_File(path, true);
    }

    public static String[] readTXT_File(String path) {

        var pathParameters = path.split("\\.");
        if (pathParameters.length < 2) {
            throw new IllegalArgumentException("Caminho do ficheiro incorreto");
        }
        String extension = pathParameters[pathParameters.length - 1];
        if (!extension.contains("txt")) {
            throw new IllegalArgumentException("Apenas aceita ficheiros TXT.");
        }

        return read_File(path, false);
    }

    private static String[] read_File(String path, boolean ignorarPrimeiraLinha) {
        try {
            String[] fileData = new String[10000];
            int i = 0;

            // Read from file
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            if (ignorarPrimeiraLinha)
                myReader.nextLine(); // Ingore first line
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                fileData[i] = data;
                i++;
            }
            myReader.close();

            // Conver List to array
            String[] array = Arrays.copyOfRange(fileData, 0, i);
            return array;
        } catch (IOException exception) {
            System.out.println("Erro: " + exception.getMessage());
        }

        return null;
    }

    public static void escreverEmFicheiro(String caminhoFicheiro, String[] info) {
        try {
            File file = new File(caminhoFicheiro);
            boolean newFile = file.createNewFile();

            OutputStream os = new FileOutputStream(file, true);
            OutputStreamWriter w = new OutputStreamWriter(os, StandardCharsets.UTF_8);
            BufferedWriter bw = new BufferedWriter(w);

            PrintWriter out = new PrintWriter(bw);
            for (String linha : info) {
                out.println(linha);
            }
            out.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
