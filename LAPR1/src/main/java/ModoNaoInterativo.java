import java.time.LocalDate;

public class ModoNaoInterativo {

    public static final String OPCAO_RESOLUCAO_TEMPORAL = "-r";
    public static final String OPCAO_DATA_INICIAL = "-di";
    public static final String OPCAO_DATA_FINAL = "-df";
    public static final String OPCAO_DATA_INICIAL_INTERVALO1 = "-di1";
    public static final String OPCAO_DATA_FINAL_INTERVALO1 = "-df1";
    public static final String OPCAO_DATA_INICIAL_INTERVALO2 = "-di2";
    public static final String OPCAO_DATA_FINAL_INTERVALO2 = "-df2";

    public static final String OPCAO_PREVISAO_DADOS = "-T";

    public static String caminhoFicheiroAcumulados = "";
    public static String caminhoFicheiroTotais = "";
    public static String caminhoFicheiroMatrizTransicoes = "";
    public static String caminhoFicheiroOutput = "";

    public static boolean analiseResolcaoTemporal = false;
    public static int resolucaoTemporal = -1;
    public static LocalDate dataInicial;
    public static LocalDate dataFinal;

    public static boolean analiseComparativa = false;
    public static LocalDate dataInicial1;
    public static LocalDate dataFinal1;
    public static LocalDate dataInicial2;
    public static LocalDate dataFinal2;

    public static boolean previsaoDados = false;
    public static LocalDate dataPrevisao;

    public static void interpretarArgumentos(String[] args) {
        String[] fileData;
        try {
            for (int i = 0; i < args.length; i++) {
                switch (args[i]) {
                    case OPCAO_RESOLUCAO_TEMPORAL -> {
                        analiseResolcaoTemporal = true;
                        i++;
                        try {
                            resolucaoTemporal = Integer.parseInt(args[i]);
                        } catch (NumberFormatException n) {
                            throw new IllegalArgumentException("A opção de resolução tem que ser um número\n" +
                                    "0 -> diária;\n" +
                                    "1 -> semanal\n" +
                                    "2 -> mensal\n");
                        }
                    }
                    case OPCAO_DATA_INICIAL -> {
                        analiseResolcaoTemporal = true;
                        i++;
                        dataInicial = Utils.conververStringParaDate(args[i]);
                    }
                    case OPCAO_DATA_FINAL -> {
                        analiseResolcaoTemporal = true;
                        i++;
                        dataFinal = Utils.conververStringParaDate(args[i]);
                    }
                    case OPCAO_PREVISAO_DADOS -> {
                        previsaoDados = true;
                        i++;
                        dataPrevisao = Utils.conververStringParaDate(args[i]);
                    }
                    case OPCAO_DATA_INICIAL_INTERVALO1 -> {
                        analiseComparativa = true;
                        i++;
                        dataInicial1 = Utils.conververStringParaDate(args[i]);
                    }
                    case OPCAO_DATA_FINAL_INTERVALO1 -> {
                        analiseComparativa = true;
                        i++;
                        dataFinal1 = Utils.conververStringParaDate(args[i]);
                    }
                    case OPCAO_DATA_INICIAL_INTERVALO2 -> {
                        analiseComparativa = true;
                        i++;
                        dataInicial2 = Utils.conververStringParaDate(args[i]);
                    }
                    case OPCAO_DATA_FINAL_INTERVALO2 -> {
                        analiseComparativa = true;
                        i++;
                        dataFinal2 = Utils.conververStringParaDate(args[i]);
                    }
                    default -> {
                        int numFicheiros = args.length - i;
                        switch (numFicheiros) {
                            case 4 -> {
                                caminhoFicheiroTotais = args[i];
                                fileData = FileReader.readCSV_File(caminhoFicheiroTotais);
                                // Load all the information
                                if (fileData != null)
                                    DataComparerDadosTotais.loadData(fileData);

                                caminhoFicheiroAcumulados = args[i + 1];
                                fileData = FileReader.readCSV_File(caminhoFicheiroAcumulados);
                                // Load all the information
                                if (fileData != null)
                                    DataComparerDadosAcumulados.loadData(fileData);

                                caminhoFicheiroMatrizTransicoes = args[i + 2];
                                fileData = FileReader.readTXT_File(caminhoFicheiroMatrizTransicoes);
                                // Load all the information
                                if (fileData != null)
                                    CalculoMatrizes.loadMatrizTransicoesEstado(fileData);

                                caminhoFicheiroOutput = args[i + 3];
                            }
                            case 3 -> {
                                caminhoFicheiroTotais = args[i];
                                fileData = FileReader.readCSV_File(caminhoFicheiroTotais);
                                // Load all the information
                                if (fileData != null)
                                    DataComparerDadosTotais.loadData(fileData);

                                caminhoFicheiroMatrizTransicoes = args[i + 1];
                                fileData = FileReader.readTXT_File(caminhoFicheiroMatrizTransicoes);
                                // Load all the information
                                if (fileData != null)
                                    CalculoMatrizes.loadMatrizTransicoesEstado(fileData);

                                caminhoFicheiroOutput = args[i + 2];
                            }
                            case 2 -> {
                                caminhoFicheiroAcumulados = args[i];
                                fileData = FileReader.readCSV_File(caminhoFicheiroAcumulados);
                                // Load all the information
                                if (fileData != null)
                                    DataComparerDadosAcumulados.loadData(fileData);
                                caminhoFicheiroOutput = args[i + 1];
                            }
                            default -> {
                                throw new IllegalArgumentException("Ficheiros não seguem o formato especificado");
                            }
                        }
                        i = args.length;
                    }
                }
            }
            //
            if (caminhoFicheiroOutput.length() > 0) {
                if (analiseResolcaoTemporal)
                    analiseTemporal();

                if (analiseComparativa)
                    analiseComparativa();

                if (previsaoDados)
                    previsaoDados();
            } else {
                System.out.println("Caminho do ficheiro de output não especficado");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void analiseTemporal() {
        if (resolucaoTemporal >= 0 && resolucaoTemporal <= 2) {
            String[] output = new String[0];
            switch (resolucaoTemporal) {
                case 0:
                    if (caminhoFicheiroAcumulados.length() > 0) {
                        output = DataComparerDadosAcumulados.analisarResolucaoDiaria(dataInicial, dataFinal);
                        guardarOutput(output);
                    }
                    if (caminhoFicheiroTotais.length() > 0) {
                        output = DataComparerDadosTotais.analisarResolucaoDiaria(dataInicial, dataFinal);
                        guardarOutput(output);
                    }
                    break;
                case 1:
                    if (caminhoFicheiroAcumulados.length() > 0) {
                        output = DataComparerDadosAcumulados.analisarPorSemana(dataInicial, dataFinal);
                        guardarOutput(output);
                    }
                    if (caminhoFicheiroTotais.length() > 0) {
                        output = DataComparerDadosTotais.analisarPorSemana(dataInicial, dataFinal);
                        guardarOutput(output);
                    }
                    break;
                case 2:
                    if (caminhoFicheiroAcumulados.length() > 0) {
                        output = DataComparerDadosAcumulados.analisarPorMes(dataInicial, dataFinal);
                        guardarOutput(output);
                    }
                    if (caminhoFicheiroTotais.length() > 0) {
                        output = DataComparerDadosTotais.analisarPorMes(dataInicial, dataFinal);
                        guardarOutput(output);
                    }
                    break;
                default:
                    break;
            }
        } else {
            System.out.println("Parametro -X da resolução temporal tem que ser um numero entre 0 e 2");
        }
    }

    private static void analiseComparativa() {
        String[] output = new String[0];
        if (dataInicial1 != null && dataFinal1 != null && dataInicial2 != null && dataFinal2 != null) {
            if (caminhoFicheiroAcumulados.length() > 0) {
                output = DataComparerDadosAcumulados.analisarComparativamente(dataInicial1, dataFinal1, dataInicial2, dataFinal2);
                guardarOutput(output);
            }
            if (caminhoFicheiroTotais.length() > 0) {
                output = DataComparerDadosTotais.analisarComparativamente(dataInicial1, dataFinal1, dataInicial2, dataFinal2);
                guardarOutput(output);
            }
        } else {
            System.out.println("Comparação necessita da data inicial e final para ambos os intervalos de tempo");
        }
    }

    private static void previsaoDados() {
        String[] output = new String[0];

        if (caminhoFicheiroMatrizTransicoes.length() > 0) {
            if (caminhoFicheiroTotais.length() > 0) {
                if (dataPrevisao != null) {
                    System.out.println("Previsao");
                    output = PrevisaoDados.previsao(dataPrevisao);
                    guardarOutput(output);
                } else {
                    System.out.println("Previsão de dados necessita de uma data através do parametro -T.");
                }
            } else {
                System.out.println("Previsão de dados necessita do ficheiro com os dados totais.");
            }
            //TODO verificar
            output = PrevisaoDados.numeroDiasAteMorte();
            guardarOutput(output);
        } else {
            System.out.println("Previsão de dados necessita do ficheiro com a matriz de transições.");
        }
    }

    private static void guardarOutput(String[] output) {
        FileReader.escreverEmFicheiro(caminhoFicheiroOutput, output);
    }
}
