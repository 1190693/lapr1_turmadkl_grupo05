import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Utils {
    /**
     * Percorre o array com toda a informação e procura por uma linha com a data desejada
     *
     * @param data data que se pretende
     * @return a informação para a data desejada
     */
    public static DataUnit encontrarInfoPorData(LocalDate data, DataUnit[] allData) {
        DataUnit info = null;
        for (DataUnit infoPorData : allData) {
            if (infoPorData.date.equals(data)) {
                info = infoPorData;
            }
        }
        return info;
    }

    /**
     * Percorre o array com toda a informação e procura por uma linha com a data desejada
     *
     * @param data data que se pretende
     * @return a informação para a data desejada
     */
    public static int encontrarPosicaoInfoPorData(LocalDate data, DataUnit[] allData) {

        for (int i = 0; i < allData.length; i++) {
            if (allData[i].date.equals(data)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Divide todos os valores de DataUnit por uma constante
     *
     * @param info   DataUnit com os valores
     * @param numero constante
     * @return novo DataUnit com os valores da divisao
     */
    public static DataUnit dividirInfoPorNumero(DataUnit info, int numero) {
        DataUnit divisao = new DataUnit(0, 0, 0, 0, 0);
        divisao.deaths = info.deaths / numero;
        divisao.infected = info.infected / numero;
        divisao.hospitalized = info.hospitalized / numero;
        divisao.dailyNotInfected = info.dailyNotInfected / numero;
        divisao.UCI_Hospitalized = info.UCI_Hospitalized / numero;
        return divisao;
    }

    /**
     * Multiplicar os valores de dataUnit1 por os valores de dataUnit2 respetivos
     *
     * @param dataUnit1 info que vai ser subtraida
     * @param dataUnit2 info que vai subtrair
     * @return DataUnit com os valores da subtraçao
     */
    public static DataUnit multiplicarInfos(DataUnit dataUnit1, DataUnit dataUnit2) {
        DataUnit produto = new DataUnit(0, 0, 0, 0, 0);
        produto.deaths = dataUnit1.deaths * dataUnit2.deaths;
        produto.infected = dataUnit1.infected * dataUnit2.infected;
        produto.hospitalized = dataUnit1.hospitalized * dataUnit2.hospitalized;
        produto.dailyNotInfected = dataUnit1.dailyNotInfected * dataUnit2.dailyNotInfected;
        produto.UCI_Hospitalized = dataUnit1.UCI_Hospitalized * dataUnit2.UCI_Hospitalized;
        return produto;
    }

    /**
     * Somar os valores de dataUnit1 por os valores de dataUnit2 respetivos
     *
     * @param dataUnit1 info que vai ser subtraida
     * @param dataUnit2 info que vai subtrair
     * @return DataUnit com os valores da subtraçao
     */
    public static DataUnit somarInfo(DataUnit dataUnit1, DataUnit dataUnit2) {
        DataUnit soma = new DataUnit(0, 0, 0, 0, 0);
        soma.deaths = dataUnit1.deaths + dataUnit2.deaths;
        soma.infected = dataUnit1.infected + dataUnit2.infected;
        soma.hospitalized = dataUnit1.hospitalized + dataUnit2.hospitalized;
        soma.dailyNotInfected = dataUnit1.dailyNotInfected + dataUnit2.dailyNotInfected;
        soma.UCI_Hospitalized = dataUnit1.UCI_Hospitalized + dataUnit2.UCI_Hospitalized;
        return soma;
    }

    /**
     * Subtrair os valores de dataUnit1 por os valores de dataUnit2 respetivos
     *
     * @param dataUnit1 info que vai ser subtraida
     * @param dataUnit2 info que vai subtrair
     * @return DataUnit com os valores da subtraçao
     */
    public static DataUnit subtrairInfo(DataUnit dataUnit1, DataUnit dataUnit2) {
        DataUnit diferenca = new DataUnit(0, 0, 0, 0, 0);
        diferenca.deaths = dataUnit1.deaths - dataUnit2.deaths;
        diferenca.infected = dataUnit1.infected - dataUnit2.infected;
        diferenca.hospitalized = dataUnit1.hospitalized - dataUnit2.hospitalized;
        diferenca.dailyNotInfected = dataUnit1.dailyNotInfected - dataUnit2.dailyNotInfected;
        diferenca.UCI_Hospitalized = dataUnit1.UCI_Hospitalized - dataUnit2.UCI_Hospitalized;
        return diferenca;
    }

    /**
     * Verifica se a dataInicio é antes da dataFim
     *
     * @param dataInicio
     * @param dataFim
     * @return
     */
    public static boolean verificarDatas(LocalDate dataInicio, LocalDate dataFim) {
        return !dataInicio.isAfter(dataFim) && !dataInicio.equals(dataFim);
    }

    public static LocalDate conververStringParaDate(String dataString) {
        String[] arr = dataString.split("-");
        DateTimeFormatter formatter = null;
        if (arr[0].length() == 4) {
            formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        } else {
            formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        }

        formatter = formatter.withLocale(Locale.ENGLISH);
        LocalDate data = LocalDate.parse(dataString, formatter);
        return data;
    }

    public static int diferencaEmDias(LocalDate dataFinal, LocalDate dataInicial) {
        int numeroDias = 0;
        LocalDate data = dataInicial;
        while (data.isBefore(dataFinal)) {
            data = data.plusDays(1);
            numeroDias++;
        }
        return numeroDias;
    }

    public static DataUnit[] loadData(String[] dataArray) {
        final int DATE_POSITION = 0;
        final int NOT_INFECTED_DAILY_POSITION = 1;
        final int INFECTED_POSITION = 2;
        final int HOSPITALIZED_POSITION = 3;
        final int UCI_HOSPITALIZED_POSITION = 4;
        final int DEATH_POSITION = 5;

        DataUnit[] allData = new DataUnit[dataArray.length];
        String[] line;
        for (int i = 0; i < dataArray.length; i++) {
            String dataLine = dataArray[i];
            line = dataLine.split(",");

            String date = line[DATE_POSITION];
            int notInfected = Integer.parseInt(line[NOT_INFECTED_DAILY_POSITION]);
            int infected = Integer.parseInt(line[INFECTED_POSITION]);
            int hospitalized = Integer.parseInt(line[HOSPITALIZED_POSITION]);
            int UCI_Hospitalized = Integer.parseInt(line[UCI_HOSPITALIZED_POSITION]);
            int deaths = Integer.parseInt(line[DEATH_POSITION]);

            var dataUnit = new DataUnit(date, notInfected, infected, hospitalized, UCI_Hospitalized, deaths);
            allData[i] = dataUnit;
        }
        return allData;
    }

    public static DataUnit encontrarPrimeiraData(DataUnit[] allData) {
        DataUnit infoPrimeiraData = allData[0];
        for (int i = 0; i < allData.length; i++) {
            if (allData[i].date.isBefore(infoPrimeiraData.date)) {
                infoPrimeiraData = allData[i];
            }
        }
        return infoPrimeiraData;
    }

    public static DataUnit encontrarUltimaData(DataUnit[] allData) {
        DataUnit infoUltimaData = allData[0];
        for (int i = 0; i < allData.length; i++) {
            if (allData[i].date.isAfter(infoUltimaData.date)) {
                infoUltimaData = allData[i];
            }
        }
        return infoUltimaData;
    }

    public static DataUnit raizQuadrada(DataUnit info) {
        DataUnit raiz = new DataUnit(0, 0, 0, 0, 0);
        raiz.deaths = (float) Math.sqrt(info.deaths);
        raiz.infected = (float) Math.sqrt(info.infected);
        raiz.hospitalized = (float) Math.sqrt(info.hospitalized);
        raiz.dailyNotInfected = (float) Math.sqrt(info.dailyNotInfected);
        raiz.UCI_Hospitalized = (float) Math.sqrt(info.UCI_Hospitalized);
        return raiz;
    }

    public static DataUnit mediaArrayTotal(DataUnit arrayTotais[]) {
        DataUnit somatorio = new DataUnit(0, 0, 0, 0, 0);
        for (int i = 0; i < arrayTotais.length; i++) {
            somatorio = Utils.somarInfo(somatorio, arrayTotais[i]);
        }
        DataUnit media = Utils.dividirInfoPorNumero(somatorio, arrayTotais.length);

        return media;
    }

    public static DataUnit desvioPadraoDiferencas(DataUnit arrays[],DataUnit media) {
        DataUnit somatorio = new DataUnit(0, 0, 0, 0, 0);
        for (int i = 0; i < arrays.length; i++) {
            DataUnit diferenca = Utils.subtrairInfo(arrays[i],media);
            diferenca = Utils.multiplicarInfos(diferenca,diferenca);
            somatorio = Utils.somarInfo(somatorio,diferenca);
        }
        DataUnit desvioPadrao = Utils.dividirInfoPorNumero(somatorio,arrays.length);
        return Utils.raizQuadrada(desvioPadrao);
    }
}
