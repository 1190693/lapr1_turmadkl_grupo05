import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

public class DataComparerDadosTotais {


    public static DataUnit[] allData = new DataUnit[0];
    public static DataUnit infoPrimeiraData;
    public static DataUnit infoUltimaData;


    public static void loadData(String[] dataArray) {
        allData = Utils.loadData(dataArray);
        infoPrimeiraData = Utils.encontrarPrimeiraData(allData);
        infoUltimaData = Utils.encontrarUltimaData(allData);
    }

    public static String[] analisarPorIntervaloTempo(LocalDate dataInicio, LocalDate dataFim) {
        if (dataInicio.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("\nData " + dataInicio + " antecede a gama de datas carregadas");
        } else if (dataFim.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("\nData " + dataFim + " é posterior à gama de datas carregadas");

        }

        if (!Utils.verificarDatas(dataInicio, dataFim)) {
            throw new IllegalArgumentException("\nData inicial " + dataInicio +
                    " não anteceda a data final " + dataFim);
        }
        String[] output = new String[6];

        try {
            int posicaoInfoInicial = Utils.encontrarPosicaoInfoPorData(dataInicio, allData);
            int posicaoInfoFinal = Utils.encontrarPosicaoInfoPorData(dataFim, allData);

            DataUnit infoPorIntervaloTempo = new DataUnit(0, 0, 0, 0, 0);
            for (int i = posicaoInfoInicial; i <= posicaoInfoFinal; i++) {
                DataUnit infoPorData = allData[i];
                infoPorIntervaloTempo = Utils.somarInfo(infoPorIntervaloTempo, infoPorData);
            }

            String data = MostrarInformacao.mostrarIntervaloTempo(dataInicio, dataFim);
            String[] texto = MostrarInformacao.mostarInformacaoDadosTotais(infoPorIntervaloTempo, Menu.menuMostrarEstadosTotal());
            String titulo = "Análise por Intervalo de Tempo";

            output = MostrarInformacao.formarOutput(titulo, data, texto);

        } catch (DateTimeParseException d) {
            System.out.println(d.getMessage());
        }

        return output;

    }

    public static void analisarPorDiaNovosCasos(LocalDate dia) {
        try {
            DataUnit infoDiario = Utils.encontrarInfoPorData(dia, allData);

            MostrarInformacao.mostrarDataDiario(dia);
            MostrarInformacao.mostrarInformacaoNovosCasos(infoDiario, Menu.menuMostrarEstadosNovosCasos());

        } catch (DateTimeParseException d) {
            System.out.println(d.getMessage());
        }

    }

    //TODO
    public static String[] analisarPorDiaCasosAtivos(LocalDate dia) {
        if (dia.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dia + " antecede a gama de datas carregadas");
        } else if (dia.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dia + " é posterior à gama de datas carregadas");

        }

        String[] output;
        DataUnit infoDiario = Utils.encontrarInfoPorData(dia, allData);

        String data = MostrarInformacao.mostarDataDadosTotais(dia);
        String[] valores = MostrarInformacao.mostarInformacaoDadosTotais(infoDiario, Menu.menuMostrarEstadosTotal());
        String titulo = "Análise do dia";
        output = MostrarInformacao.formarOutput(titulo, data, valores);

        return output;
    }

    public static String[] analisarPorSemana(LocalDate dataInicio, LocalDate dataFim) {
        if (dataInicio.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicio + " antecede a gama de datas carregadas");
        } else if (dataFim.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFim + " é posterior à gama de datas carregadas");

        }
        if (!Utils.verificarDatas(dataInicio, dataFim)) {
            throw new IllegalArgumentException("\nData inicial " + dataInicio +
                    " não anteceda a data final " + dataFim);
        }

        int opcaoOutput = Menu.menuMostrarEstadosTotal();

        String[] output;
        String[] valores = new String[10000];
        int linhasOutput = 0, contadorSemanas = 0;

        int primeiroDiaInt = dataInicio.getDayOfWeek().getValue();

        LocalDate primeiroDiaSemana;
        LocalDate ultimoDiaSemana;

        if (primeiroDiaInt == 1) {
            primeiroDiaSemana = dataInicio;
            ultimoDiaSemana = primeiroDiaSemana.plusDays(6);
        } else {
            primeiroDiaSemana = dataInicio.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            ultimoDiaSemana = primeiroDiaSemana.plusDays(6);
        }

        int posicaoInicial = Utils.encontrarPosicaoInfoPorData(primeiroDiaSemana, allData);
        int posicaoFinal = Utils.encontrarPosicaoInfoPorData(dataFim, allData);

        int contadorDias = 0;
        DataUnit infoPorSemana = new DataUnit(0, 0, 0, 0, 0);
        for (int i = posicaoInicial; i <= posicaoFinal; i++) {
            DataUnit infoPorDia = allData[i];
            if ((infoPorDia.date.isAfter(primeiroDiaSemana) || infoPorDia.date.equals(primeiroDiaSemana)) &&
                    (infoPorDia.date.isBefore(ultimoDiaSemana) || infoPorDia.date.equals(ultimoDiaSemana))) {
                contadorDias++;
                infoPorSemana = Utils.somarInfo(infoPorSemana, infoPorDia);
            }

            if (infoPorDia.date.equals(ultimoDiaSemana)) {
                if (contadorDias == 7) {
                    String semana = MostrarInformacao.mostrarSemana(primeiroDiaSemana, ultimoDiaSemana);

                    String[] valoresSemana = MostrarInformacao.mostarInformacaoDadosTotais(infoPorSemana, opcaoOutput);

                    valores[linhasOutput] = semana;
                    linhasOutput++;
                    for (String valor : valoresSemana) {
                        valores[linhasOutput] = valor;
                        linhasOutput++;
                    }
                    contadorSemanas++;
                }

                primeiroDiaSemana = primeiroDiaSemana.plusDays(7);
                ultimoDiaSemana = ultimoDiaSemana.plusDays(7);
                infoPorSemana = new DataUnit(0, 0, 0, 0, 0);
                contadorDias = 0;
            }
        }
        if (contadorSemanas == 0) {
            throw new IllegalArgumentException("Não existe semana completa.");
        }
        valores = Arrays.copyOfRange(valores, 0, linhasOutput);
        String data = "Intervalo entre " + dataInicio + " e " + dataFim;
        output = MostrarInformacao.formarOutput("Análise Resolução Semanal", data, valores);

        return output;
    }

    public static String[] analisarPorMes(LocalDate dataInicio, LocalDate dataFim) {
        if (dataInicio.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicio + " antecede a gama de datas carregadas");
        } else if (dataFim.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFim + " é posterior à gama de datas carregadas");

        }
        if (!Utils.verificarDatas(dataInicio, dataFim)) {
            throw new IllegalArgumentException("\nData inicial " + dataInicio +
                    " não anteceda a data final " + dataFim);
        }

        int opcaoOutput = Menu.menuMostrarEstadosTotal();

        String[] output;
        String[] valores = new String[10000];
        int linhasOutput = 0;

        int diaDoMes = dataInicio.getDayOfMonth();

        LocalDate ultimoDiaDoMes;
        LocalDate primeiroDiaDoMes;

        if (diaDoMes == 1) {              //verificar se o dia é o primeiro dia do mês
            ultimoDiaDoMes = dataInicio.withDayOfMonth(dataInicio.lengthOfMonth());
            primeiroDiaDoMes = dataInicio;
        } else {
            primeiroDiaDoMes = dataInicio.plusMonths(1).withDayOfMonth(1);         //senão, salta para o proximo mês
            ultimoDiaDoMes = primeiroDiaDoMes.withDayOfMonth(primeiroDiaDoMes.lengthOfMonth());
        }

        int posicaoInicial = Utils.encontrarPosicaoInfoPorData(primeiroDiaDoMes, allData);
        int posicaoFinal = Utils.encontrarPosicaoInfoPorData(dataFim, allData);

        int contadorDias = 0, contadorMeses = 0;
        DataUnit infoPorMes = new DataUnit(0, 0, 0, 0, 0);
        for (int i = posicaoInicial; i <= posicaoFinal; i++) {
            DataUnit infoPorDia = allData[i];

            if ((infoPorDia.date.isAfter(primeiroDiaDoMes) || infoPorDia.date.equals(primeiroDiaDoMes)) &&
                    (infoPorDia.date.isBefore(ultimoDiaDoMes) || infoPorDia.date.equals(ultimoDiaDoMes))) {
                contadorDias++;
                infoPorMes = Utils.somarInfo(infoPorMes, infoPorDia);
            }

            if (infoPorDia.date.equals(ultimoDiaDoMes)) {
                if (contadorDias == primeiroDiaDoMes.lengthOfMonth()) {
                    String mes = MostrarInformacao.mostrarMes(primeiroDiaDoMes);

                    String[] valoresSemana = MostrarInformacao.mostarInformacaoDadosTotais(infoPorMes, opcaoOutput);

                    valores[linhasOutput] = mes;
                    linhasOutput++;
                    for (String valor : valoresSemana) {
                        valores[linhasOutput] = valor;
                        linhasOutput++;
                    }
                    contadorMeses++;

                } else {
                    System.out.println("Não existe mês completo.");
                }

                primeiroDiaDoMes = ultimoDiaDoMes.plusDays(1);
                ultimoDiaDoMes = primeiroDiaDoMes.withDayOfMonth(primeiroDiaDoMes.lengthOfMonth());
                infoPorMes = new DataUnit(0, 0, 0, 0, 0);
                contadorDias = 0;
            }
        }
        if (contadorMeses == 0) {
            throw new IllegalArgumentException("Não existe mês completo.");
        }
        valores = Arrays.copyOfRange(valores, 0, linhasOutput);
        String data = "Intervalo entre " + dataInicio + " e " + dataFim;
        output = MostrarInformacao.formarOutput("Análise Resolução Mensal", data, valores);

        return output;
    }

    public static String[] analisarResolucaoDiaria(LocalDate dataInicio, LocalDate dataFim) {
        if (dataInicio.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicio + " antecede a gama de datas carregadas");
        } else if (dataFim.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFim + " procede a gama de datas carregadas");
        }
        if (!Utils.verificarDatas(dataInicio, dataFim)) {
            throw new IllegalArgumentException("\nData inicial " + dataInicio +
                    " não anteceda a data final " + dataFim);
        }

        int opcaoOutput = Menu.menuMostrarEstadosTotal();

        String[] output;
        String[] valores = new String[10000];
        int linhasOutput = 0;

        for (int i = 0; i < allData.length; i++) {
            if (allData[i].date.isAfter(dataInicio) && allData[i].date.isBefore(dataFim)) {

                String dia = MostrarInformacao.mostrarDataDiario(allData[i].date);
                String[] valoresDia = MostrarInformacao.mostarInformacaoDadosTotais(allData[i], opcaoOutput);


                valores[linhasOutput] = dia;
                linhasOutput++;
                for (String valor : valoresDia) {
                    valores[linhasOutput] = valor;
                    linhasOutput++;
                }
            }
        }

        valores = Arrays.copyOfRange(valores, 0, linhasOutput);
        String data = "Intervalo entre " + dataInicio + " e " + dataFim;
        output = MostrarInformacao.formarOutput("Análise Resolução Diária", data, valores);

        return output;
    }
    //=============================================================================//


    //=========================== Analise comparativa ============================//

    /***
     * Metodo para mostrar a media e o desvio padrão de um intervalo de tempo
     *
     * @param dataInicio Data inicial
     * @param dataFim Data final
     */
    public static void analisarMediaDesvioPadrao(LocalDate dataInicio, LocalDate dataFim) {
        if (dataInicio.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicio + " antecede a gama de datas carregadas");
        } else if (dataFim.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFim + " é posterior à gama de datas carregadas");

        }

        DataUnit media = mediaPorDatas(dataInicio, dataFim);
        DataUnit desvioPadrao = desvioPadraoPorDatas(dataInicio, dataFim, media);

        System.out.println("Média:");
        MostrarInformacao.mostrarInformacaoComDecimasTotal(media, Menu.menuMostrarEstadosTotal());
        System.out.println("Desvio Padrão");
        MostrarInformacao.mostrarInformacaoComDecimasTotal(desvioPadrao, Menu.menuMostrarEstadosTotal());

    }

    /**
     * Metodo que calcula a media aritmetica de Novos casos, Novas hospilatizações, Novos internados em UCI e Novas mortes para o
     * intervalo de tempo
     * <p>
     * Formula usada: https://www.gstatic.com/education/formulas2/397133473/en/arithmetic_mean.svg
     *
     * @param dataInicial LocalDate da data inicial
     * @param dataFinal   LocalDate da data final
     * @return DataUnit com a média para todos os valores
     */
    public static DataUnit mediaPorDatas(LocalDate dataInicial, LocalDate dataFinal) {

        // Obter a diferença das datas em dias
        int diferancaDias = Utils.diferencaEmDias(dataFinal, dataInicial);
        // Obter os dados para a data inicial
        DataUnit infoInicial = Utils.encontrarInfoPorData(dataInicial, allData);
        // Obter os dados para a data final
        DataUnit infoFinal = Utils.encontrarInfoPorData(dataFinal, allData);
        DataUnit somatorioInfos = new DataUnit(0, 0, 0, 0, 0);
        for (int i = 0; i < allData.length; i++) {
            DataUnit infoPorData = allData[i];
            if ((infoPorData.date.isAfter(dataInicial) || infoPorData.date.equals(dataInicial)) &&
                    (infoPorData.date.isBefore(dataFinal) || infoPorData.date.equals(dataFinal))) {
                somatorioInfos = Utils.somarInfo(somatorioInfos, infoPorData);
            }
        }

        // Calcúlo da média usando a expressão media=soma de novos valores / numero de dias
        DataUnit media = Utils.dividirInfoPorNumero(somatorioInfos, diferancaDias);

        return media;
    }

    /**
     * Metodo que calcula o desvio padrão de Novos casos, Novas hospilatizações, Novos internados em UCI e Novas mortes
     * para o intervalo de tempo, usando a media previamente calculada
     * <p>
     * Formula usada: https://www.gstatic.com/education/formulas2/397133473/en/population_standard_deviation.svg
     *
     * @param dataInicial LocalDate da data inicial
     * @param dataFinal   LocalDate da data final
     * @param media       DataUnit com a média para todos os valores
     * @return DataUnit com o desvio padrão para todos os valores
     */
    public static DataUnit desvioPadraoPorDatas(LocalDate dataInicial, LocalDate dataFinal, DataUnit media) {
        // Obter a diferença das datas em dias (denominador da formula usada)
        int diferancaDias = Utils.diferencaEmDias(dataFinal, dataInicial);


        DataUnit somatorio = new DataUnit(0, 0, 0, 0, 0);
        for (int i = 0; i < allData.length; i++) {
            DataUnit infoPorData = allData[i];
            if ((infoPorData.date.isAfter(dataInicial) || infoPorData.date.equals(dataInicial)) &&
                    (infoPorData.date.isBefore(dataFinal) || infoPorData.date.equals(dataFinal))) {
                DataUnit diferencaComMedia = Utils.subtrairInfo(infoPorData, media);
                // Quadrado da diferença calculada na linha anteriar (xi - μ)^2
                DataUnit quadradoDiferencaComMedia = Utils.multiplicarInfos(diferencaComMedia, diferencaComMedia);
                // Adicionar o quadrado da diferença ao somatorio
                somatorio = Utils.somarInfo(somatorio, quadradoDiferencaComMedia);
            }
        }

        // Retornar a divisao do somatorio com o numero de dias
        return Utils.dividirInfoPorNumero(somatorio, diferancaDias);
    }


    public static String[] analisarComparativamente(LocalDate dataInicial1, LocalDate dataFinal1, LocalDate dataInicial2, LocalDate dataFinal2) {

        if (dataInicial1.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicial1 + " antecede a gama de datas carregadas");
        } else if (dataFinal1.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFinal1 + " é posterior à gama de datas carregadas");
        } else if (dataInicial2.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicial2 + " antecede a gama de datas carregadas");
        } else if (dataFinal2.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFinal2 + " é posterior à gama de datas carregadas");

        }
        if (!Utils.verificarDatas(dataInicial1, dataFinal1)) {
            throw new IllegalArgumentException("\nData inicial " + dataInicial1 +
                    " não anteceda a data final " + dataFinal1);
        } else if (!Utils.verificarDatas(dataInicial2, dataFinal2)) {
            throw new IllegalArgumentException("\nData inicial " + dataInicial2 +
                    " não anteceda a data final " + dataFinal2);
        }

        int opcaoOutput = Menu.menuMostrarEstadosTotal();

        int intervalo1 = Utils.diferencaEmDias(dataFinal1, dataInicial1) + 1;
        int intervalo2 = Utils.diferencaEmDias(dataFinal2, dataInicial2) + 1;
        int contDias;

        if (intervalo1 < intervalo2) {
            contDias = intervalo1;
        } else {
            contDias = intervalo2;
        }

        String[] output;
        String[] valores = new String[10000];
        int linhasOutput = 0;

        int diasIncremento = 0;
        LocalDate data1 = dataInicial1;
        LocalDate data2 = dataInicial2;
        DataUnit[] arrayTotais = new DataUnit[contDias];
        while (diasIncremento < contDias) {
            DataUnit dataUnit1 = Utils.encontrarInfoPorData(data1, allData);
            DataUnit dataUnit2 = Utils.encontrarInfoPorData(data2, allData);

            dataUnit1 = Utils.subtrairInfo(dataUnit1, dataUnit2);

            String data = MostrarInformacao.mostrarDatasDiferenca(data1, data2);
            //System.out.println("Diferença entre: " + data1.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + " e " + data2.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
            String[] valoresDiferenca = MostrarInformacao.mostarInformacaoDadosTotais(dataUnit1, opcaoOutput);

            valores[linhasOutput] = data;
            linhasOutput++;
            for (String valor : valoresDiferenca) {
                valores[linhasOutput] = valor;
                linhasOutput++;
            }

            arrayTotais[diasIncremento] = dataUnit1;
            diasIncremento++;
            data1 = data1.plusDays(1);
            data2 = data2.plusDays(1);


        }
        valores[linhasOutput] = "\n----------------------";
        linhasOutput++;
        valores[linhasOutput] = "Média";
        linhasOutput++;

        DataUnit media = Utils.mediaArrayTotal(arrayTotais);
        String[] mediaOutput = MostrarInformacao.mostrarInformacaoComDecimasTotal(media, opcaoOutput);
        for (String valor : mediaOutput) {
            valores[linhasOutput] = valor;
            linhasOutput++;
        }
        valores[linhasOutput] = "\n----------------------";
        linhasOutput++;
        valores[linhasOutput] = "Desvio Padrão";
        linhasOutput++;
        DataUnit desvioPadrao = Utils.desvioPadraoDiferencas(arrayTotais, media);
        String[] desvioOutput = MostrarInformacao.mostrarInformacaoComDecimasTotal(desvioPadrao, opcaoOutput);
        for (String valor : desvioOutput) {
            valores[linhasOutput] = valor;
            linhasOutput++;
        }

        valores = Arrays.copyOfRange(valores, 0, linhasOutput);
        output = MostrarInformacao.formarOutput("Comparação Temporal", valores);

        return output;
    }

}
