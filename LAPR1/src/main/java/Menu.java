import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class Menu {
    private static final Scanner scanner = new Scanner(System.in);
    private static boolean ficheiroAcumuladosLido = false;
    private static boolean ficheiroTotaisLido = false;
    private static boolean ficheiroMatrizLido = false;
    public static boolean modoIterativo = false;

    public static void run() {
        modoIterativo = true;
        int option = -1;

        while (option != 0) {
            showOptions();
            option = readOption();
            switch (option) {
                case 1:
                    readFile();
                    break;
                case 2:
                    analisarTemporalMenu();
                    break;
                case 3:
                    analisarComapartivaTemporal();
                    break;
                case 4:
                    preverDados();
                    break;
                case 0:
                    System.out.println("A sair...");
                    break;
                default:
                    System.out.println("Por favor escolha uma das opções possíveis");
            }
        }
    }

    private static void showOptions() {
        System.out.println("=================");
        System.out.println("=== 1: Ler Ficheiro ===");
        System.out.println("=== 2: Análise de dados por intervalo temporal ===");
        System.out.println("=== 3: Análise comparativa de dois intervalos de tempo ===");
        System.out.println("=== 4: Previsão de dados ===");
        System.out.println("=== 0: Sair ===");
        System.out.println("=================\n");
    }

    private static int readOption() {
        System.out.println("\n=================");
        try {
            System.out.println("Escolha uma opção");
            String value = lerInput();
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            System.out.println("A opção tem que ser um número.");
        }
        return -1;
    }

    private static String lerInput() {
        String value = scanner.nextLine();
        return value;
    }

    private static void readFile() {
        int option = -1;
        String path;
        while (option != 0) {
            System.out.println("\n=================");
            System.out.println("==== Menu ler ficheiros ===");
            System.out.println("=== 1: Ler ficheiro CSV com dados acumulados ===");
            System.out.println("=== 2: Ler ficheiro CSV com dados totais ===");
            System.out.println("=== 3: Ler ficheiro TXT com a matriz de transições ===");
            System.out.println("=== 0: Sair ===");
            System.out.println("=================\n");
            option = readOption();
            try {
                switch (option) {
                    case 1:
                        System.out.println("Escreva o caminho do ficheiro csv pretendido.");
                        path = scanner.nextLine();
                        String[] fileData = FileReader.readCSV_File(path);
                        // Load all the information
                        if (fileData != null) {
                            ficheiroAcumuladosLido = true;
                            DataComparerDadosAcumulados.loadData(fileData);
                        }
                        break;
                    case 2:
                        System.out.println("Escreva o caminho do ficheiro csv pretendido.");
                        path = scanner.nextLine();
                        fileData = FileReader.readCSV_File(path);
                        // Load all the information
                        if (fileData != null) {
                            ficheiroTotaisLido = true;
                            DataComparerDadosTotais.loadData(fileData);
                        }
                        break;
                    case 3:
                        System.out.println("Escreva o caminho do ficheiro txt pretendido.");
                        path = scanner.nextLine();
                        fileData = FileReader.readTXT_File(path);
                        // Load all the information
                        if (fileData != null) {
                            ficheiroMatrizLido = true;
                            CalculoMatrizes.loadMatrizTransicoesEstado(fileData);
                        }
                        break;
                    case 0:
                        break;
                    default:
                        System.out.println("Por favor escolha uma das opções possíveis");
                }
            } catch (IllegalArgumentException a) {
                System.out.println(a.getMessage());
            }
        }
        System.out.println("\n=================");
    }

    private static void preverDados() {
        int option = -1;
        int ficheiroEscoliho;
        String[] output = new String[1];
        while (option != 0) {
            System.out.println("\n=================");
            System.out.println("==== Menu previsão de dados ===");
            System.out.println("=== 1: Estimativa de casos ===");
            System.out.println("=== 2: Estimativa de dias até à morte ===");
            System.out.println("=== 0: Sair ===");
            System.out.println("=================\n");
            option = readOption();
            if (ficheiroMatrizLido) {
                switch (option) {
                    case 1:
                        if (ficheiroTotaisLido) {
                            System.out.println("== Data de previsão ==");
                            LocalDate dataInicial = lerintervaloTempo();
                            try {
                                output = PrevisaoDados.previsao(dataInicial);
                            } catch (IllegalArgumentException i) {
                                System.out.println(i.getMessage());
                                output = new String[0];
                                break;
                            } finally {
                                for (String s : output) {
                                    System.out.println(s);
                                }
                                pressionarParaAvancar();
                            }
                            guardarEmFicheiro(output);
                        } else {
                            System.out.println("Ficheiro com os dados totais de cada dia ainda não foi carregado na aplicação");
                        }
                        break;
                    case 2:
                        output = PrevisaoDados.numeroDiasAteMorte();
                        for (String s : output) {
                            System.out.println(s);
                        }
                        pressionarParaAvancar();
                        guardarEmFicheiro(output);
                    case 0:
                        break;
                    default:
                        System.out.println("Por favor escolha uma das opções possíveis");
                }
            } else {
                System.out.println("Ficheiro com a matriz de transições ainda não foi carregado na aplicação");
            }
        }
    }

    private static void analisarTemporalMenu() {
        int option = -1;
        LocalDate dataInicial;
        LocalDate dataFinal;
        int ficheiroEscoliho;
        String[] output = new String[1];

        while (option != 0) {
            System.out.println("\n=================");
            System.out.println("==== Menu análise temporal ===");
            System.out.println("=== 1: Análise de um determinado dia ===");
            System.out.println("=== 2: Análise com diferentes resoluções temporais ===");
            System.out.println("=== 3: Análise por intervalo de tempo ===");
            System.out.println("=== 0: Sair ===");
            System.out.println("=================\n");
            option = readOption();
            switch (option) {
                case 1:
                    menuAnaliseDiaria();
                    break;
                case 2:
                    menuResolucoesTemporais();
                    break;

                case 3:
                    ficheiroEscoliho = escolherFicheiroUsar();
                    if (ficheiroEscoliho != 0) {
                        System.out.println("== Intervalo tempo inicial ==");
                        dataInicial = lerintervaloTempo();
                        System.out.println("== Intervalo tempo final ==");
                        dataFinal = lerintervaloTempo();
                        try {
                            if (ficheiroEscoliho == 1) {
                                output = DataComparerDadosAcumulados.analisarPorIntervaloTempo(dataInicial, dataFinal);
                            } else if (ficheiroEscoliho == 2) {
                                output = DataComparerDadosTotais.analisarPorIntervaloTempo(dataInicial, dataFinal);
                            }
                        } catch (IllegalArgumentException i) {
                            System.out.println(i.getMessage());
                            output = new String[0];
                            break;
                        } finally {
                            for (String s : output) {
                                System.out.println(s);
                            }
                            pressionarParaAvancar();
                        }
                        guardarEmFicheiro(output);
                    }
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Por favor escolha uma das opções possiveis");
            }
        }
    }

    private static void menuAnaliseDiaria() {
        int option = -1;
        LocalDate dia;
        int ficheiroEscoliho;
        String[] output = new String[0];
        while (option != 0) {
            System.out.println("\n=================");
            System.out.println("==== Menu análise diária ===");
            System.out.println("=== 1: Novos casos ===");
            System.out.println("=== 2: Casos totais ativos ===");
            System.out.println("=== 0: Sair ===");
            System.out.println("=================\n");
            option = readOption();
            switch (option) {
                case 1:
                    if (ficheiroAcumuladosLido) {
                        System.out.println("== Dia ==");
                        dia = lerintervaloTempo();
                        try {
                            output = DataComparerDadosAcumulados.analisarPorDiaNovosCasos(dia);
                        } catch (IllegalArgumentException i) {
                            System.out.println(i.getMessage());
                            output = new String[0];
                            break;
                        } finally {
                            for (String s : output) {
                                System.out.println(s);
                            }
                            pressionarParaAvancar();
                        }
                        guardarEmFicheiro(output);
                    } else {
                        System.out.println("Ficheiro com os dados acumulados ainda não foi carregado na aplicação");
                    }
                    break;
                case 2:
                    if (ficheiroTotaisLido) {
                        System.out.println("== Dia ==");
                        dia = lerintervaloTempo();
                        try {
                            output = DataComparerDadosTotais.analisarPorDiaCasosAtivos(dia);
                        } catch (IllegalArgumentException i) {
                            System.out.println(i.getMessage());
                            output = new String[0];
                            break;
                        } finally {
                            for (String s : output) {
                                System.out.println(s);
                            }
                            pressionarParaAvancar();
                        }
                        guardarEmFicheiro(output);

                    } else {
                        System.out.println("Ficheiro com os dados totais de cada dia ainda não foi carregado na aplicação");
                    }
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Por favor escolha uma das opções possíveis");
            }
        }
    }

    private static void analisarComapartivaTemporal() {
        int ficheiroEscoliho;
        String[] output = new String[0];

        ficheiroEscoliho = escolherFicheiroUsar();
        if (ficheiroEscoliho != 0) {

            System.out.println();
            System.out.println("Primeiro intervalo de tempo.");
            System.out.println("\n=================");
            System.out.println("== Data Inicial ==");
            LocalDate dataInicial1 = lerintervaloTempo();
            System.out.println("== Data Final ==");
            LocalDate dataFinal1 = lerintervaloTempo();
            System.out.println("\n=================");
            System.out.println();
            System.out.println("Segundo intervalo de tempo.");
            System.out.println("\n=================");
            System.out.println("== Data Inicial ==");
            LocalDate dataInicial2 = lerintervaloTempo();
            System.out.println("== Data Final ==");
            LocalDate dataFinal2 = lerintervaloTempo();
            System.out.println("\n=================");

            try {
                if (ficheiroEscoliho == 1) {
                    output = DataComparerDadosAcumulados.analisarComparativamente(dataInicial1, dataFinal1, dataInicial2, dataFinal2);
                } else if (ficheiroEscoliho == 2) {
                    output = DataComparerDadosTotais.analisarComparativamente(dataInicial1, dataFinal1, dataInicial2, dataFinal2);
                }
            } catch (IllegalArgumentException i) {
                System.out.println(i.getMessage());
                output = new String[0];
            } finally {
                for (String s : output) {
                    System.out.println(s);
                }
                pressionarParaAvancar();
            }
            guardarEmFicheiro(output);
        }

    }


    public static LocalDate lerintervaloTempo() {
        try {
            System.out.println("Formato (yyyy-MM-dd ou dd-MM-yyyy): ");
            String dataString = lerInput();
            LocalDate data = Utils.conververStringParaDate(dataString);

            return data;
        } catch (DateTimeParseException d) {
            System.out.println("=====================");
            System.out.println("ERRO: " + d.getMessage());
            System.out.println("Por favor reintroduza a data");
            System.out.println("=====================\n");
        }

        return lerintervaloTempo();
    }


    private static void pressionarParaAvancar() {
        System.out.println("ENTER para continuar");
        scanner.nextLine();
    }

    /**
     * @return 1 para ficheiro acumulados, 2 para ficheiro de totais e 0 para nenhum
     */
    private static int escolherFicheiroUsar() {
        int escolha = 0;
        int option = -1;
        while (option != 0) {
            System.out.println("\n=================");
            System.out.println("==== Escolha a funcionalidade pretendida ===");
            System.out.println("=== 1: Novos casos ===");
            System.out.println("=== 2: Total de casos ativos ===");
            System.out.println("=== 0: Sair ===");
            System.out.println("=================\n");
            option = readOption();
            switch (option) {
                case 1:
                    if (!ficheiroAcumuladosLido) {
                        System.out.println("Ficheiro com os dados acumulados ainda não foi carregado na aplicação");
                    } else {
                        return 1;
                    }
                    break;
                case 2:
                    if (!ficheiroTotaisLido) {
                        System.out.println("Ficheiro com os dados totais de cada dia ainda não foi carregado na aplicação");
                    } else {
                        return 2;
                    }
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Por favor escolha uma das opções possíveis");
            }
        }


        return escolha;
    }

    public static void menuResolucoesTemporais() {
        int option = -1;
        int ficheiroEscoliho;
        LocalDate dataInicial;
        LocalDate dataFinal;
        String[] output = new String[0];
        while (option != 0) {
            System.out.println("\n=================");
            System.out.println("==== Menu Resoluções Temporais ===");
            System.out.println("=== 1: Análise diária ===");
            System.out.println("=== 2: Análise semanal ===");
            System.out.println("=== 3: Análise mensal ===");
            System.out.println("=== 0: Sair ===");
            System.out.println("=================\n");
            option = readOption();
            switch (option) {
                case 1:
                    ficheiroEscoliho = escolherFicheiroUsar();
                    if (ficheiroEscoliho != 0) {
                        System.out.println("== Intervalo tempo inicial ==");
                        dataInicial = lerintervaloTempo();
                        System.out.println("== Intervalo tempo final ==");
                        dataFinal = lerintervaloTempo();
                        try {
                            if (ficheiroEscoliho == 1) {
                                output = DataComparerDadosAcumulados.analisarResolucaoDiaria(dataInicial, dataFinal);
                            } else if (ficheiroEscoliho == 2) {
                                output = DataComparerDadosTotais.analisarResolucaoDiaria(dataInicial, dataFinal);
                            }
                        } catch (IllegalArgumentException i) {
                            System.out.println(i.getMessage());
                            output = new String[0];
                            break;
                        } finally {
                            for (String s : output) {
                                System.out.println(s);
                            }
                            pressionarParaAvancar();
                        }
                        guardarEmFicheiro(output);
                    }
                    break;
                case 2:
                    ficheiroEscoliho = escolherFicheiroUsar();
                    if (ficheiroEscoliho != 0) {
                        System.out.println("== Intervalo tempo inicial ==");
                        dataInicial = lerintervaloTempo();
                        System.out.println("== Intervalo tempo final ==");
                        dataFinal = lerintervaloTempo();
                        try {
                            if (ficheiroEscoliho == 1) {
                                output = DataComparerDadosAcumulados.analisarPorSemana(dataInicial, dataFinal);
                            } else if (ficheiroEscoliho == 2) {
                                output = DataComparerDadosTotais.analisarPorSemana(dataInicial, dataFinal);
                            }
                        } catch (IllegalArgumentException i) {
                            System.out.println(i.getMessage());
                            output = new String[0];
                            break;
                        } finally {
                            for (String s : output) {
                                System.out.println(s);
                            }
                            pressionarParaAvancar();
                        }
                        guardarEmFicheiro(output);
                    }
                    break;
                case 3:
                    ficheiroEscoliho = escolherFicheiroUsar();
                    if (ficheiroEscoliho != 0) {
                        System.out.println("== Intervalo tempo inicial ==");
                        LocalDate dataInicialMes = lerintervaloTempo();
                        System.out.println("== Intervalo tempo final ==");
                        LocalDate datafinalMes = lerintervaloTempo();
                        try {
                            if (ficheiroEscoliho == 1) {
                                output = DataComparerDadosAcumulados.analisarPorMes(dataInicialMes, datafinalMes);
                            } else if (ficheiroEscoliho == 2) {
                                output = DataComparerDadosTotais.analisarPorMes(dataInicialMes, datafinalMes);
                            }
                        } catch (IllegalArgumentException i) {
                            System.out.println(i.getMessage());
                            output = new String[0];
                            break;
                        } finally {
                            for (String s : output) {
                                System.out.println(s);
                            }
                            pressionarParaAvancar();
                        }
                        guardarEmFicheiro(output);
                    }
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Por favor escolha uma das opções possíveis");
            }
        }
    }

    public static void guardarEmFicheiro(String[] info) {
        int option = -1;
        while (option != 0) {
            System.out.println("=================");
            System.out.println("==== Quer guardar num ficheiro? ===");
            System.out.println("=== 1: Sim ===");
            System.out.println("=== 2: Não ===");
            System.out.println("=================");
            option = readOption();
            switch (option) {
                case 1:
                    System.out.println("Escreva o caminho do ficheiro onde quer guardar:");
                    String caminho = lerInput();
                    FileReader.escreverEmFicheiro(caminho, info);
                    return;
                case 2:
                    return;
                default:
                    System.out.println("Por favor escolha uma das opções possíveis");
            }
        }
    }

    public static int menuMostrarEstadosTotal() {
        if (modoIterativo) {
            int option = -1;
            while (option != 0) {
                System.out.println("\n=================");
                System.out.println("=== Qual estado pretende visualizar? ===");
                System.out.println("=== 1: Infetados ===");
                System.out.println("=== 2: Hospitalizações ===");
                System.out.println("=== 3: Internados em UCI ===");
                System.out.println("=== 4: Óbitos ===");
                System.out.println("=== 5: Todos os estados ===");
                System.out.println("=================\n");
                option = readOption();
                if (option <= 6 && option >= 1)
                    return option;
            }
            return option;
        } else {
            return 6;
        }
    }

    public static int menuMostrarEstadosNovosCasos() {
        if (modoIterativo) {
            int option = -1;
            while (option != 0) {
                System.out.println("\n=================");
                System.out.println("=== Qual estado pretende visualizar? ===");
                System.out.println("=== 1: Casos ===");
                System.out.println("=== 2: Hospitalizações ===");
                System.out.println("=== 3: Internados em UCI ===");
                System.out.println("=== 4: Óbitos ===");
                System.out.println("=== 5: Todos os estados ===");
                System.out.println("=================\n");
                option = readOption();
                if (option <= 6 && option >= 1)
                    return option;
            }
            return option;
        } else {
            return 6;
        }
    }

    public static int menuMostrarEstadosPrevisao() {
        if (modoIterativo) {
            int option = -1;
            while (option != 0) {
                System.out.println("\n=================");
                System.out.println("=== Qual estado pretende visualizar? ===");
                System.out.println("=== 1: Não infetados ===");
                System.out.println("=== 2: Infetados ===");
                System.out.println("=== 3: Hospitalizações ===");
                System.out.println("=== 4: Internados em UCI ===");
                System.out.println("=== 5: Óbitos ===");
                System.out.println("=== 6: Todos os estados ===");
                System.out.println("=================\n");
                option = readOption();
                if (option <= 6 && option >= 1)
                    return option;
            }
            return option;
        } else {
            return 6;
        }
    }
}
