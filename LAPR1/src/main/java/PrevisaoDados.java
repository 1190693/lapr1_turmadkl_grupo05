import java.time.LocalDate;
import java.util.Arrays;

public class PrevisaoDados {
    public static String[] previsao(LocalDate diaPrevisao) {
        if (diaPrevisao.isBefore(DataComparerDadosTotais.infoPrimeiraData.date)
                || diaPrevisao.equals(DataComparerDadosTotais.infoPrimeiraData.date)) {
            throw new IllegalArgumentException("A data de previsão não pode anteceder ou ser igual à primeira data carregada");
        }
        DataUnit informacao = encontrarDiaMaisProximo(diaPrevisao);

        float[][] dadosDiaMaisProximo = diferencaDias(informacao.date, diaPrevisao);

        String data = MostrarInformacao.mostraPrevisaoDadosData(diaPrevisao);
        float[][] dadosDiaPrevisao = calcularDadosdiaPrevisao(informacao, dadosDiaMaisProximo);

        String[] valores = MostrarInformacao.mostrarInformacaoPrevisaoDados(dadosDiaPrevisao, Menu.menuMostrarEstadosPrevisao());
        String[] output = MostrarInformacao.formarOutput("Previsão de dados", data, valores);


        return output;
    }

    public static DataUnit encontrarDiaMaisProximo(LocalDate data) {
        DataUnit ultimoDia;
        ultimoDia = DataComparerDadosTotais.allData[DataComparerDadosTotais.allData.length - 1];
        if (ultimoDia.date.isBefore(data)) {
            return ultimoDia;
        } else {
            for (int i = DataComparerDadosTotais.allData.length - 1; i >= 0; i--) {
                if (DataComparerDadosTotais.allData[i].date.isBefore(data)) {
                    return DataComparerDadosTotais.allData[i];
                }
            }
        }
        return null;
    }

    public static float[][] diferencaDias(LocalDate ultimoDia, LocalDate diaDePrevisao) {
        int diferancaDias = Utils.diferencaEmDias(diaDePrevisao, ultimoDia);
        float[][] matriz = CalculoMatrizes.matrizTransicoesEstado;
        for (int i = 1; i < diferancaDias; i++) {
            matriz = CalculoMatrizes.multiplicarMatriz(matriz, CalculoMatrizes.matrizTransicoesEstado);
        }
        return matriz;
    }

    public static float[][] calcularDadosdiaPrevisao(DataUnit diaMaisProximo, float[][] matriz) {
        float[][] matrizDiaMaisProximo = new float[5][1];
        matrizDiaMaisProximo[0][0] = diaMaisProximo.dailyNotInfected;
        matrizDiaMaisProximo[1][0] = diaMaisProximo.infected;
        matrizDiaMaisProximo[2][0] = diaMaisProximo.hospitalized;
        matrizDiaMaisProximo[3][0] = diaMaisProximo.UCI_Hospitalized;
        matrizDiaMaisProximo[4][0] = diaMaisProximo.deaths;
        float[][] dadosDiaPrevisao = CalculoMatrizes.multiplicarMatriz(matriz, matrizDiaMaisProximo);

        return dadosDiaPrevisao;
    }

    public static String[] numeroDiasAteMorte() {
        float[][] matrizN = calcularMatrizN(CalculoMatrizes.matrizTransicoesEstado);

        float[][] matrizM = calcularMatrizM(matrizN);

        String[] output = MostrarInformacao.mostrarDiasAteMorte(matrizM);

        return output;
    }

    public static float[][] calcularMatrizN(float[][] matrizTransicoes) {
        float[][] matrizN;
        float[][] matrizIdentidade = new float[matrizTransicoes.length - 1][matrizTransicoes[0].length - 1];
        float[][] matrizQ = new float[matrizTransicoes.length - 1][matrizTransicoes[0].length - 1];

        for (int i = 0; i < matrizTransicoes.length - 1; i++) {
            for (int j = 0; j < matrizTransicoes[i].length - 1; j++) {
                matrizQ[i][j] = matrizTransicoes[i][j];

                if (i == j) {
                    matrizIdentidade[i][j] = 1;
                }
            }
        }

        float[][] matrizDiferenca = CalculoMatrizes.subtrairMatrizes(matrizIdentidade, matrizQ);

        float[][][] matrizesLU = CalculoMatrizes.decomporMatrizComMetodoCrout(matrizDiferenca);

        float[][] inversaMatrizL = CalculoMatrizes.matrizInversa(matrizesLU[0]);
        float[][] inversaMatrizU = CalculoMatrizes.matrizInversa(matrizesLU[1]);
        matrizN = CalculoMatrizes.multiplicarMatriz(inversaMatrizU, inversaMatrizL);

        return matrizN;
    }

    public static float[][] calcularMatrizM(float[][] matrizN) {
        float[][] matrizM;
        float[][] vetorLinha = new float[1][matrizN[0].length];
        Arrays.fill(vetorLinha[0], 1);

        matrizM = CalculoMatrizes.multiplicarMatriz(vetorLinha, matrizN);

        return matrizM;
    }
}
