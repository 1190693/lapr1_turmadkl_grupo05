import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class DataComparerDadosAcumulados {

    public static DataUnit[] allData = new DataUnit[0];
    public static DataUnit infoPrimeiraData;
    public static DataUnit infoUltimaData;

    public static void loadData(String[] dataArray) {
        allData = Utils.loadData(dataArray);
        infoPrimeiraData = Utils.encontrarPrimeiraData(allData);
        infoUltimaData = Utils.encontrarUltimaData(allData);
    }

    //=========================== Analise de informação ===========================//

    /**
     * Mostra o número de novos casos, o número de novas hospitalizações,
     * o número de novos internados em UCI, e o número de mortes num determinado intervalo de tempo
     *
     * @param dataInicio
     * @param dataFim
     */
    public static String[] analisarPorIntervaloTempo(LocalDate dataInicio, LocalDate dataFim) {
        if (dataInicio.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicio + " antecede a gama de datas carregadas");
        } else if (dataFim.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFim + " é posterior à gama de datas carregadas");

        }
        if (!Utils.verificarDatas(dataInicio, dataFim)) {
            throw new IllegalArgumentException("\nData inicial " + dataInicio +
                    " não anteceda a data final " + dataFim);
        }

        String[] output = new String[1];
        if (Utils.verificarDatas(dataInicio, dataFim)) {
            LocalDate dataAntesDataInicio = dataInicio.minusDays(1);

            DataUnit infoInicial = Utils.encontrarInfoPorData(dataAntesDataInicio, allData);
            DataUnit infoFinal = Utils.encontrarInfoPorData(dataFim, allData);

            infoFinal = Utils.subtrairInfo(infoFinal, infoInicial);
            String data = MostrarInformacao.mostrarIntervaloTempo(dataInicio, dataFim);
            String[] valores = MostrarInformacao.mostrarInformacaoNovosCasos(infoFinal, Menu.menuMostrarEstadosNovosCasos());

            String titulo = "Análise por Intervalo de Tempo";
            output = MostrarInformacao.formarOutput(titulo, data, valores);

        } else {
            System.out.printf("A data inicial:%s não antecede a data final: %s\n", dataInicio, dataFim);
        }
        return output;
    }

    public static String[] analisarPorDiaNovosCasos(LocalDate diario) {

        if (diario.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + diario + " antecede a gama de datas carregadas");
        } else if (diario.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + diario + " é posterior à gama de datas carregadas");

        }
        if (diario.equals(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Não existem informação suficiente para obter os novos casos do dia" + diario);
        }

        String[] output = new String[1];
        try {
            LocalDate diaAnterior = diario.minusDays(1);
            DataUnit infoDiario = Utils.encontrarInfoPorData(diario, allData);
            DataUnit infoDiaAnterior = Utils.encontrarInfoPorData(diaAnterior, allData);

            infoDiario = Utils.subtrairInfo(infoDiario, infoDiaAnterior);
            String data = MostrarInformacao.mostrarDataDiario(diario);
            String[] valores = MostrarInformacao.mostrarInformacaoNovosCasos(infoDiario, Menu.menuMostrarEstadosNovosCasos());

            String titulo = "Novos dados do dia";
            output = MostrarInformacao.formarOutput(titulo, data, valores);
        } catch (DateTimeParseException d) {
            System.out.println(d.getMessage());
        }
        return output;
    }

    public static void analisarPorDiaCasosAtivos(LocalDate diario) {
        try {
            DataUnit infoDiario = Utils.encontrarInfoPorData(diario, allData);

            MostrarInformacao.mostrarDataDiario(diario);
            MostrarInformacao.mostrarInformacaoNovosCasos(infoDiario, Menu.menuMostrarEstadosNovosCasos());

        } catch (DateTimeParseException d) {
            System.out.println(d.getMessage());
        }

    }

    public static String[] analisarPorSemana(LocalDate dataInicio, LocalDate dataFim) {
        if (dataInicio.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicio + " antecede a gama de datas carregadas");
        } else if (dataFim.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFim + " é posterior à gama de datas carregadas");
        }
        if (!Utils.verificarDatas(dataInicio, dataFim)) {
            throw new IllegalArgumentException("\nData inicial " + dataInicio +
                    " não anteceda a data final " + dataFim);
        }
        int opcaoOutput = Menu.menuMostrarEstadosNovosCasos();

        String[] output;
        String[] valores = new String[10000];
        int linhasOutput = 0, contadorSemanas = 0;

        int primeiroDiaInt = dataInicio.getDayOfWeek().getValue();
        LocalDate primeiroDiaSemana, diaAnteriorPrimeiroDiaSemana;
        LocalDate ultimoDiaSemana;

        if (primeiroDiaInt == 1) {
            primeiroDiaSemana = dataInicio;
            ultimoDiaSemana = primeiroDiaSemana.plusDays(6);
        } else {
            primeiroDiaSemana = dataInicio.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            ultimoDiaSemana = primeiroDiaSemana.plusDays(6);
        }

        while (ultimoDiaSemana.isBefore(dataFim) || ultimoDiaSemana.equals(dataFim)) {
            diaAnteriorPrimeiroDiaSemana = primeiroDiaSemana.minusDays(1);
            DataUnit infoInicial = Utils.encontrarInfoPorData(diaAnteriorPrimeiroDiaSemana, allData);
            DataUnit infoFinal = Utils.encontrarInfoPorData(ultimoDiaSemana, allData);

            if (infoFinal != null && infoInicial != null) {
                DataUnit infoPorSemana = Utils.subtrairInfo(infoFinal, infoInicial);

                String semana = MostrarInformacao.mostrarSemana(primeiroDiaSemana, ultimoDiaSemana);

                String[] valoresSemana = MostrarInformacao.mostrarInformacaoNovosCasos(infoPorSemana, opcaoOutput);

                valores[linhasOutput] = semana;
                linhasOutput++;
                for (String valor : valoresSemana) {
                    valores[linhasOutput] = valor;
                    linhasOutput++;
                }
                contadorSemanas++;
            }

            primeiroDiaSemana = primeiroDiaSemana.plusDays(7);
            ultimoDiaSemana = ultimoDiaSemana.plusDays(7);
        }
        if (contadorSemanas == 0) {
            throw new IllegalArgumentException("Não existe semana completa.");
        }

        valores = Arrays.copyOfRange(valores, 0, linhasOutput);
        String data = "Intervalo entre " + dataInicio + " e " + dataFim;
        output = MostrarInformacao.formarOutput("Análise Resolução Semanal", data, valores);

        return output;
    }

    public static String[] analisarPorMes(LocalDate dataInicio, LocalDate dataFim) {
        if (dataInicio.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicio + " antecede a gama de datas carregadas");
        } else if (dataFim.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFim + " é posterior à gama de datas carregadas");

        }
        if (!Utils.verificarDatas(dataInicio, dataFim)) {
            throw new IllegalArgumentException("\nData inicial " + dataInicio +
                    " não antecede a data final " + dataFim);
        }

        int opcaoOutput = Menu.menuMostrarEstadosNovosCasos();

        String[] output;
        String[] valores = new String[10000];
        int linhasOutput = 0, contadorMeses = 0;


        int diaDoMes = dataInicio.getDayOfMonth();

        LocalDate ultimoDiaDoMes;
        LocalDate primeiroDiaDoMes, diaAnteriorPrimeiroDiaMes;

        if (diaDoMes == 1) {              //verificar se o dia é o primeiro dia do mês
            ultimoDiaDoMes = dataInicio.withDayOfMonth(dataInicio.lengthOfMonth());
            primeiroDiaDoMes = dataInicio;
        } else {
            primeiroDiaDoMes = dataInicio.plusMonths(1).withDayOfMonth(1);         //senão, salta para o proximo mês
            ultimoDiaDoMes = primeiroDiaDoMes.withDayOfMonth(primeiroDiaDoMes.lengthOfMonth());
        }

        while (ultimoDiaDoMes.isBefore(dataFim) || ultimoDiaDoMes.equals(dataFim)) {
            diaAnteriorPrimeiroDiaMes = primeiroDiaDoMes.minusDays(1);
            DataUnit infoInicial = Utils.encontrarInfoPorData(diaAnteriorPrimeiroDiaMes, allData);
            DataUnit infoFinal = Utils.encontrarInfoPorData(ultimoDiaDoMes, allData);

            if (infoFinal != null && infoInicial != null) {
                DataUnit infoPorMes = Utils.subtrairInfo(infoFinal, infoInicial);   //cálculo diferença

                String mes = MostrarInformacao.mostrarMes(primeiroDiaDoMes);

                String[] valoresSemana = MostrarInformacao.mostrarInformacaoNovosCasos(infoPorMes, opcaoOutput);

                valores[linhasOutput] = mes;
                linhasOutput++;
                for (String valor : valoresSemana) {
                    valores[linhasOutput] = valor;
                    linhasOutput++;
                }
                contadorMeses++;
            }

            primeiroDiaDoMes = ultimoDiaDoMes.plusDays(1);
            ultimoDiaDoMes = primeiroDiaDoMes.withDayOfMonth(primeiroDiaDoMes.lengthOfMonth());
        }
        if (contadorMeses == 0) {
            throw new IllegalArgumentException("Não existe mês completo.");
        }

        valores = Arrays.copyOfRange(valores, 0, linhasOutput);
        String data = "Intervalo entre " + dataInicio + " e " + dataFim;
        output = MostrarInformacao.formarOutput("Análise Resolução Mensal", data, valores);

        return output;
    }

    public static String[] analisarResolucaoDiaria(LocalDate dataInicio, LocalDate dataFim) {
        if (dataInicio.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicio + " antecede a gama de datas carregadas");
        } else if (dataFim.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFim + " é posterior à gama de datas carregadas");

        }
        if (!Utils.verificarDatas(dataInicio, dataFim)) {
            throw new IllegalArgumentException("\nData inicial " + dataInicio +
                    " não anteceda a data final " + dataFim);
        }

        int opcaoOutput = Menu.menuMostrarEstadosNovosCasos();

        String[] output;
        String[] valores = new String[10000];
        int linhasOutput = 0;

        if (Utils.verificarDatas(dataInicio, dataFim)) {
            LocalDate dataAnteriorInicio = dataInicio.minusDays(1);

            for (int i = 0; i < allData.length; i++) {
                if ((allData[i].date.isAfter(dataAnteriorInicio) || allData[i].date.equals(dataAnteriorInicio))
                        && allData[i].date.isBefore(dataFim)) {

                    DataUnit infoPorDataAnterior = allData[i];
                    DataUnit infoPorDataAtual = allData[i + 1];

                    DataUnit infoNovos = Utils.subtrairInfo(infoPorDataAtual, infoPorDataAnterior);

                    String dia = MostrarInformacao.mostrarDataDiario(infoPorDataAtual.date);
                    String[] valoresDia = MostrarInformacao.mostrarInformacaoNovosCasos(infoNovos, opcaoOutput);


                    valores[linhasOutput] = dia;
                    linhasOutput++;
                    for (String valor : valoresDia) {
                        valores[linhasOutput] = valor;
                        linhasOutput++;
                    }
                }
            }
        } else {
            System.out.printf("A data inicial:%s não antecede a data final: %s\n", dataInicio, dataFim);
        }

        valores = Arrays.copyOfRange(valores, 0, linhasOutput);
        String data = "Intervalo entre " + dataInicio + " e " + dataFim;
        output = MostrarInformacao.formarOutput("Análise Resolução Diária", data, valores);

        return output;
    }
    //=============================================================================//


    //=========================== Analise comparativa ============================//

    /***
     * Metodo para mostrar a media e o desvio padrão de um intervalo de tempo
     *
     * @param dataInicio Data inicial
     * @param dataFim Data final
     */
    public static String[] analisarMediaDesvioPadrao(LocalDate dataInicio, LocalDate dataFim) {
        if (dataInicio.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicio + " antecede a gama de datas carregadas");
        } else if (dataFim.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFim + " é posterior à gama de datas carregadas");

        }

        List<String> texto = new ArrayList<>();
        if (Utils.verificarDatas(dataInicio, dataFim)) {
            DataUnit media = mediaPorDatas(dataInicio, dataFim);
            DataUnit desvioPadrao = desvioPadraoPorDatas(dataInicio, dataFim, media);

            String data = "\tIntervalo entre " + dataInicio + " e " + dataFim + ":";
            System.out.println(data);
            String mediaString = "Média:";
            System.out.println(mediaString);
            String[] mediaOutput = MostrarInformacao.mostrarInformacaoComDecimasNovosCasos(media, Menu.menuMostrarEstadosNovosCasos());

            String desvioString = "Desvio Padrão";
            System.out.println(desvioString);
            String[] desvioPadraoOutput = MostrarInformacao.mostrarInformacaoComDecimasNovosCasos(desvioPadrao, Menu.menuMostrarEstadosNovosCasos());

            texto.add("==================");
            texto.add("Média e desvio padrão");
            texto.add(data);
            texto.add(mediaString);
            texto.addAll(List.of(mediaOutput));
            texto.add(desvioString);
            texto.addAll(List.of(desvioPadraoOutput));
            texto.add("==================");
        } else {
            System.out.printf("A data inicial:%s não antecede a data final: %s\n", dataInicio, dataFim);
        }
        return texto.toArray(new String[0]);
    }

    /**
     * Metodo que calcula a media aritmetica de Novos casos, Novas hospilatizações, Novos internados em UCI e Novas mortes para o
     * intervalo de tempo
     * <p>
     * Formula usada: https://www.gstatic.com/education/formulas2/397133473/en/arithmetic_mean.svg
     *
     * @param dataInicial LocalDate da data inicial
     * @param dataFinal   LocalDate da data final
     * @return DataUnit com a média para todos os valores
     */
    public static DataUnit mediaPorDatas(LocalDate dataInicial, LocalDate dataFinal) {

        // Obter a diferença das datas em dias
        int diferancaDias = Utils.diferencaEmDias(dataFinal, dataInicial);


        LocalDate dataAnteriorDataInicial = dataInicial.minusDays(1);
        // Obter os dados para a data inicial
        DataUnit infoInicial = Utils.encontrarInfoPorData(dataAnteriorDataInicial, allData);
        // Obter os dados para a data final
        DataUnit infoFinal = Utils.encontrarInfoPorData(dataFinal, allData);

        // Subtrair as dados da data final com os dados da data inicial para obter os valores acumulados neste intervalo
        // de tempo. Seria a mesma coisa que adicionar os novos valores para todos os dias deste intervalo
        DataUnit infoDiferenca = Utils.subtrairInfo(infoFinal, infoInicial);

        // Calcúlo da média usando a expressão media=soma de novos valores / numero de dias
        DataUnit media = Utils.dividirInfoPorNumero(infoDiferenca, diferancaDias);

        return media;
    }

    /**
     * Metodo que calcula o desvio padrão de Novos casos, Novas hospilatizações, Novos internados em UCI e Novas mortes
     * para o intervalo de tempo, usando a media previamente calculada
     * <p>
     * Formula usada: https://www.gstatic.com/education/formulas2/397133473/en/population_standard_deviation.svg
     *
     * @param dataInicial LocalDate da data inicial
     * @param dataFinal   LocalDate da data final
     * @param media       DataUnit com a média para todos os valores
     * @return DataUnit com o desvio padrão para todos os valores
     */
    public static DataUnit desvioPadraoPorDatas(LocalDate dataInicial, LocalDate dataFinal, DataUnit media) {
        // Obter a diferença das datas em dias (denominador da formula usada)
        int diferancaDias = Utils.diferencaEmDias(dataFinal, dataInicial);

        LocalDate dataAtual = dataInicial;
        LocalDate dataAnterior = dataInicial.minusDays(1);

        // Somatorio do quadrado da diferença entre os valores da data atual e a media ( Numerador da formula usada)
        DataUnit somatorio = new DataUnit(0, 0, 0, 0, 0);

        // Percorrer todos os valores para datas anteriros à data final
        while (dataAtual.isBefore(dataFinal) || dataAtual.equals(dataFinal)) {
            // Obter os dados do dia anterior
            DataUnit infoAnterior = Utils.encontrarInfoPorData(dataAnterior, allData);

            if (infoAnterior == null) {
                throw new IllegalArgumentException(String.format("Não existem dados para o dia %s\n", dataAnterior.toString()));
            }

            // Obter os dados do dia atual do ciclo
            DataUnit infoAtual = Utils.encontrarInfoPorData(dataAtual, allData);

            // Subtrair os dados acumulados do dia atual e os dados acumulados do dia anterior para obter os dados
            // novos do dia atual
            DataUnit diferanca = Utils.subtrairInfo(infoAtual, infoAnterior);

            // Subtrair os dados novos do dia atual com a media (xi - μ)
            DataUnit diferencaComMedia = Utils.subtrairInfo(diferanca, media);
            // Quadrado da diferença calculada na linha anteriar (xi - μ)^2
            DataUnit quadradoDiferencaComMedia = Utils.multiplicarInfos(diferencaComMedia, diferencaComMedia);
            // Adicionar o quadrado da diferença ao somatorio
            somatorio = Utils.somarInfo(somatorio, quadradoDiferencaComMedia);

            // Atualizar os dias para dia anterior ser o dia atual e dia atual ser o dia seguinte
            dataAnterior = dataAtual;
            dataAtual = dataAtual.plusDays(1);
        }

        // Retornar a divisao do somatorio com o numero de dias
        return Utils.dividirInfoPorNumero(somatorio, diferancaDias);
    }

    public static String[] analisarComparativamente(LocalDate dataInicial1, LocalDate dataFinal1, LocalDate dataInicial2, LocalDate dataFinal2) {
        if (dataInicial1.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicial1 + " antecede a gama de datas carregadas");
        } else if (dataFinal1.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFinal1 + " é posterior à gama de datas carregadas");
        } else if (dataInicial2.isBefore(infoPrimeiraData.date)) {
            throw new IllegalArgumentException("Data " + dataInicial2 + " antecede a gama de datas carregadas");
        } else if (dataFinal2.isAfter(infoUltimaData.date)) {
            throw new IllegalArgumentException("Data " + dataFinal2 + " é posterior à gama de datas carregadas");

        }
        if (!Utils.verificarDatas(dataInicial1, dataFinal1)) {
            throw new IllegalArgumentException("\nData inicial " + dataInicial1 +
                    " não anteceda a data final " + dataFinal1);
        } else if (!Utils.verificarDatas(dataInicial2, dataFinal2)) {
            throw new IllegalArgumentException("\nData inicial " + dataInicial2 +
                    " não anteceda a data final " + dataFinal2);
        }

        int opcaoOutput = Menu.menuMostrarEstadosNovosCasos();

        int intervalo1 = Utils.diferencaEmDias(dataFinal1, dataInicial1) + 1;
        int intervalo2 = Utils.diferencaEmDias(dataFinal2, dataInicial2) + 1;
        int contDias;

        if (intervalo1 < intervalo2) {
            contDias = intervalo1;
        } else {
            contDias = intervalo2;
        }

        String[] output;
        String[] valores = new String[10000];
        int linhasOutput = 0;

        int diasIncremento = 0;
        LocalDate data1 = dataInicial1;
        LocalDate data2 = dataInicial2;
        DataUnit[] arrayTotais = new DataUnit[contDias];
        while (diasIncremento < contDias) {
            DataUnit dataUnit1 = Utils.encontrarInfoPorData(data1, allData);
            DataUnit dataUnit2 = Utils.encontrarInfoPorData(data2, allData);

            DataUnit dataUnitAntes1 = Utils.encontrarInfoPorData(data1.minusDays(1), allData);
            DataUnit dataUnitAntes2 = Utils.encontrarInfoPorData(data2.minusDays(1), allData);

            dataUnit1 = Utils.subtrairInfo(dataUnit1, dataUnitAntes1);
            dataUnit2 = Utils.subtrairInfo(dataUnit2, dataUnitAntes2);

            dataUnit1 = Utils.subtrairInfo(dataUnit1, dataUnit2);

            String data = MostrarInformacao.mostrarDatasDiferenca(data1, data2);
            String[] valoresDiferenca = MostrarInformacao.mostrarInformacaoNovosCasos(dataUnit1, opcaoOutput);

            valores[linhasOutput] = data;
            linhasOutput++;
            for (String valor : valoresDiferenca) {
                valores[linhasOutput] = valor;
                linhasOutput++;
            }

            arrayTotais[diasIncremento] = dataUnit1;

            diasIncremento++;
            data1 = data1.plusDays(1);
            data2 = data2.plusDays(1);
        }

        valores[linhasOutput] = "\n----------------------";
        linhasOutput++;
        valores[linhasOutput] = "Média";
        linhasOutput++;

        DataUnit media = Utils.mediaArrayTotal(arrayTotais);
        String[] mediaOutput = MostrarInformacao.mostrarInformacaoComDecimasNovosCasos(media, opcaoOutput);
        for (String valor : mediaOutput) {
            valores[linhasOutput] = valor;
            linhasOutput++;
        }
        valores[linhasOutput] = "\n----------------------";
        linhasOutput++;
        valores[linhasOutput] = "Desvio Padrão";
        linhasOutput++;
        DataUnit desvioPadrao = Utils.desvioPadraoDiferencas(arrayTotais, media);
        String[] desvioOutput = MostrarInformacao.mostrarInformacaoComDecimasNovosCasos(desvioPadrao, opcaoOutput);
        for (String valor : desvioOutput) {
            valores[linhasOutput] = valor;
            linhasOutput++;
        }

        valores = Arrays.copyOfRange(valores, 0, linhasOutput);
        output = MostrarInformacao.formarOutput("Comparação Temporal", valores);

        return output;
    }
}
