import java.time.LocalDate;

public class DataUnit {
    public LocalDate date;
    public float dailyNotInfected;
    public float infected;
    public float hospitalized;
    public float UCI_Hospitalized;
    public float deaths;

    public DataUnit(String date, int dailyNotInfected, int infected, int hospitalized, int UCI_Hospitalized, int deaths) {
        this.date = Utils.conververStringParaDate(date);

        this.dailyNotInfected = dailyNotInfected;
        this.infected = infected;
        this.hospitalized = hospitalized;
        this.UCI_Hospitalized = UCI_Hospitalized;
        this.deaths = deaths;
    }

    public DataUnit(int dailyNotInfected, int infected, int hospitalized, int UCI_Hospitalized, int deaths) {

        this.date = null;

        this.dailyNotInfected = dailyNotInfected;
        this.infected = infected;
        this.hospitalized = hospitalized;
        this.UCI_Hospitalized = UCI_Hospitalized;
        this.deaths = deaths;
    }

    @Override
    public String toString() {
        return "DataUnit{" +
                "date='" + date.toString() + '\'' +
                ", dailyNotInfected=" + dailyNotInfected +
                ", infected=" + infected +
                ", hospitalized=" + hospitalized +
                ", UCI_Hospitalized=" + UCI_Hospitalized +
                ", deaths=" + deaths +
                '}';
    }
}
